#ifndef SUPERWEAPON_H_INCLUDED
#define SUPERWEAPON_H_INCLUDED

#define I_SUPERWEAPON "superweapon-1"

#define SUPERWEAPONS 1

typedef enum Superweapon
{
	SUPERWEAPON_NUKE
} Superweapon;

typedef struct Isuperweapon
{
	INTERFACE_HEAD_DECL

	void (*NukeFireSequence)(Player *p, int refundCredits, int freqChargeRequired);
	int (*GetChargeFreq)(Arena *arena, int freq, Superweapon superweapon);
	int (*GetChargePlayer)(Player *p, Superweapon superweapon);
	void (*StartGame)(Arena *arena);
	void (*StopGame)(Arena *arena);

} Isuperweapon;

#endif
