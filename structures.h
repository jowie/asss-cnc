#ifndef __STRUCTURES_H
#define __STRUCTURES_H
#include <stdbool.h>
#include "swappablegame.h"

/*
 * structures v2.1 by JoWie (rewritten from structures v1 by smong)
 *
 * Destructible structures you can add to your map. once one team has lost all
 * their structures the game ends.
 *
*/

/** How many freqs are there; Freqs outside this range can still have structures, but will have less features; Do not use freqs over this range in interfaces */
#define STRUCTURES_FREQS 3

/** How many freqs are playing in the structures game; */
#define STRUCTURES_PLAYINGFREQS 2

/** How many structure types are there */
#define STRUCTURES_STRUCTURETYPES 12

/** The maximum number of structures in an arena */
#define STRUCTURES_MAXIMUMSTRUCTURES 200

/** How often structures refresh; This value effects the repair rate */
#define STRUCTURES_STRUCTUREREFRESH_TIMER 200


#define I_STRUCTURES "structures-3"
#define I_SWAPPABLEGAME_STRUCTURES "sgame-structures-2"



#if STRUCTURES_PLAYINGFREQS != 2
#error "Only 2 playing freqs are supported"
#endif

#if STRUCTURES_FREQS < STRUCTURES_PLAYINGFREQS
#error "STRUCTURES_FREQS must be greater or equal to STRUCTURES_PLAYINGFREQS"
#endif

#if STRUCTURES_STRUCTURETYPES < 1
#error "A minimum of 1 structure type is required"
#endif

/* All of this should be read-only for other modules */


typedef enum STRUCTURETYPE
{
        CONSTRUCTIONYARD = 0,
        POWERPLANT,
        OREREFINERY,
        BARRACKS,
        WARFACTORY,
        AIRFIELD,
        RADAR,
        TECH,
        SILO,
        REPAIR,
        NUKE,
        SECONDSUPERWEAPON // = 11

} STRUCTURETYPE;

static const char * const structureTypeNames[STRUCTURES_STRUCTURETYPES] =
{
        "Construction Yard",
        "Power Plant",
        "Ore Refinery",
        "Barracks",
        "Warfactory",
        "Airfield",
        "Radar",
        "Tech",
        "Silo",
        "Repair",
        "Nuke",
        "Second Super Weapon"
};

/** STRUCTURESTATE is which state the on map lvz is on */
typedef enum STRUCTURESTATE
{
        STRUCTURESTATE_HIDDEN = 0,  /// Invisible
        STRUCTURESTATE_DESTROYED,   /// Destroyed

        //anything > DESTROYED means alive
        STRUCTURESTATE_FULLHEALTH,  /// 50%-100%
        STRUCTURESTATE_HALFHEALTH,  /// 25% - 50%
        STRUCTURESTATE_FOURTHHEALTH /// 0-25%

        //Note the structure state can jump to and from any points. Ex FULLHEALTH -> FOURTHHEALTH is valid
} STRUCTURESTATE;


struct structureData
{
        Arena *arena;
        const char *typeID;

        STRUCTURESTATE structureLVZState; /// Which state is LVZ in (as it appears on players screen)

        int health; /// How much health has this structure; This is only set by SpawnStructure() and DestroyStructure() and SetHealth()
        bool alive; /// Is the structure alive?; This is only set by SpawnStructure() and DestroyStructure()
        ticks_t lastDeath; ///The last time the structure has been destroyed;
        double respawnTicksLeft; /// How many respawn ticks left? this is lowered every 2 seconds. This method is not very accurate, however that is not an issue

        int hpbar_objectid; /// The object id of the lvz hpbar
        int hpbar_oldImageoffset; /// The old offset for the lvz hpbar;
        bool hpbar_visible; /// Is the hpbar currently visible on screen?

        int spawnCount; ///Overall spawn count
        int destroyCount; ///Overall destroy count

        struct structureData_config
        {
                Region *region; ///Region to use for damage

                int freq; ///The freq that owns this structure

                int x, y; ///objmove for objectid(50|25), and the coordinate to use for any offset. Also used to send sound to nearby players
                int width, height; ///optional, used for explosion_range and sounds

                int objectidLT100;///Object to show when the structure is not destroyed (50%-100%); -1 = disabled
                int objectidLT50;///Object to show when the structure is not destroyed (25%-50%);
                int objectidLT25; ///Object to show when the structure is not destroyed (0-25%);


                int destroyedObjectid; ///Object to replace objectid(25|50) with when destroyed; -1 = disabled
                int destroyedXOffset, destroyedYOffset; ///objmove for destroyed_objectid

                int respawnSound; /// sound to play when respawned; sent to nearby players; 0 = disabled
                int damaged50to49Sound;/// Sound played when structure goes to 50% health; sent to nearby players; 0 = disabled
                int damaged25to24Sound; /// Sound played when structure goes to 25% health; sent to nearby players; 0 = disabled


                int initialHealth; ///Health of the building

                int explosionObjectid; ///Additional Object to show when destroyed; -1 = disabled
                int explosionSound; ///Sound played when destroyed; 0 = disabled
                int explosionXOffset, explosionYOffset; ///objmove for explosion_objectid

                int explosionDamageRange; /// How far to prize -charge when destroyed; 0 = disabled
                int soundRange; /// How far to play any sounds generated by this structure in pixels


                int repairLT100; ///How many hp to repair every 2 ticks (50%-100%)
                int repairLT50; ///How many hp to repair every 2 ticks (25-50%)
                int repairLT25; ///How many hp to repair every 2 ticks (0-25%)

                int respawnTicks; ///how often to respawn; 0 = dont

                int rewardBonus; ///reward bonus if structure lives (in permille)

                bool hpbar_enabled; ///Use a hpbar for this structure?

                bool isBase; ///only valid for team 0 and 1. 1 = this structure has to be destroyed for win the other team to win.
                bool gameDependent; ///0 = structure lives outside the main game.
		bool spawnOnStart;
                bool teamDamage; /// can be hurt by its own freq

                unsigned long int structureType; ///The type of this structure, bitwise. Example: sdata->structure_type |= pow2(CONSTRUCTIONYARD)

                char *respawnEnemyMessage;
                char *explosionEnemyMessage;
                char *damaged50to49EnemyMessage;
                char *damaged25to24EnemyMessage;

                char *respawnTeamMessage;
                char *explosionTeamMessage;
                char *damaged50to49TeamMessage;
                char *damaged25to24TeamMessage;

        } config;
};

struct structureTypeData
{
        Arena *arena; ///Arena this structure type belongs to
        int freq; ///Freq this structure type belongs to

        STRUCTURETYPE type; /// The actual structure type

        int structureCount; ///How much structures are currently alive with this type;
        int structureCountInitial; ///How much structures exist in total with this type

        int structureCountLVZ; ///Same as above, however this is the count that was used to display the LVZ that is currently on screen; -1 = LVZ has not been sent yet; This variable is only set by UpdateStructureFx(...)
        int structureCountOld; ///This is the structure count that was used for the last call back; This variable is only set by UpdateStructureType(...)

        int structureTotalHealth; ///The total health of structures with this type
        int structureTotalInitialHealth; ///The total initial health of structures with this type

        struct structureTypeData_config
        {
                int ZeroToOneObjectid;
                int OneToZeroObjectid;

                int ZeroToOneEnemySound;
                int ZeroToOneTeamSound;
                int OneToZeroEnemySound;
                int OneToZeroTeamSound;

                char *ZeroToOneEnemyMessage;
                char *ZeroToOneTeamMessage;
                char *OneToZeroEnemyMessage;
                char *OneToZeroTeamMessage;
        } config;
};

struct structures_freqData
{
        Arena *arena;
        int freq;

        ticks_t lastUnderAttack; ///time of the last under attack sond and message

        int structureCount; /// How many alive structures with IsBase does each freq have
        int structureCountInitial;  /// How many structures with IsBase does each freq have

        int structureTotalInitialHealth; /// How much maximum health of the structures with IsBase does each freq have
        int structureTotalHealth; /// How much health of the structures with IsBase does each freq have

        unsigned int repairRate; /// How fast to repair in permille; 0 = structures dont repair; 1000 = structures repair at the rate defined in their settings
        unsigned int respawnRate;/// How fast to respawn in permille; 0 = structures dont respawn; 1000 = structures respawn at the rate defined in their settings

        int rewardBonus; ///Bonus for winning in permille

        int percentageDisplay_percentageLVZ; /// The percentage that was used to display the LVZ currently on screen

        struct structures_freqData_config
        {
                char *freqName; /// The name of the freq
                char *underAttackMessage; ///Message sent when a structure is being attacked
                int underAttackSound; ///Sound played when a structure is being attacked; 0 = disabled
                int underAttackReplayTicks; ///How often to resend the message and sound; 0 = Under attack message and sound disabled

                int percentageDisplay_imageStart; /// Images used for the numbers 0 - 9
                int percentageDisplay_imagePercentage; /// Images used for the percentage symbol
                int percentageDisplay_objectStart; /// 4 Objects used to display the percentage
        } config;
};

struct structures_arenaData
{
        bool running; /// Is the game running?
        ticks_t gameStartTime;
        ticks_t gameStopTime;

        //struct structureData *sdata; ///Array containing all the  structure data. TODO: HashTable
        //int sdataCount, sdataAllocated;
        HashTable *structures; /// struct structureData*

        GameoverFunc gameOverFunc; /// Function to call when the game is over; This is set from sgm_cnc

        int hpbar_objectTicker; ///How many hpbar object ids have been allocated

        struct structureTypeData structureTypes[STRUCTURES_FREQS][STRUCTURES_STRUCTURETYPES]; /// Holds info about all the structure types
        struct structures_freqData freqs[STRUCTURES_FREQS];

        bool initialized; /// Has this arena been initialized?

        Target tgt; /// A target union to this arena

        /** Config read from settings; These values should not be modified */
        struct structures_arenaData_config
        {
                bool hideScreenObjectsOnGameStop;

                int extraReward; ///Extra reward to give when winning
                int reward; /// The reward to give (playing * playing * reward / 1000 + extraReward)
                bool useJackpot; ///Add the ?jackpot to the reward
                bool splitPoints; ///split the points between freq members
                int gameRunningObjectid; ///Object to show while the game is running
                bool weightedDamage; /// If a player shoots at a building; the actual damage done = damage / players on his freq

                int hpbar_objectstart, hpbar_objectsallocated; ///What objects to use
                int hpbar_imagestart, hpbar_imagesallocated; /// What images to use
                int hpbar_xoffset, hpbar_yoffset; ///The offset of the hpbars
        } config;
};
typedef const struct structureData structureData;
typedef const struct structureTypeData structureTypeData;
typedef const struct structures_freqData structures_freqData;
typedef const struct structures_arenaData structures_arenaData;

typedef struct Istructures
{
        INTERFACE_HEAD_DECL

        void (*SetRespawnRate)(Arena *arena, int freq, unsigned int value);
        void (*SetRepairRate)(Arena *arena, int freq, unsigned int value);

        int  (*SetHealth)(structureData *sdata, int health, bool silent); /// Set the health of a structure, setting the health to 0 (or lower) is the same as calling DestroyStructure(). Do not call this when sdata->alive == false
        void (*SpawnStructure)(structureData *sdata, bool silent); /// Spawn a structure. Do not call this when sdata->alive == true
        void (*DestroyStructure)(structureData *sdata, bool silent); /// Destroy a structure. Do not call this when sdata->alive == false

        structures_arenaData* (*GetArenaData)(Arena *arena); /// Gets the arena data for the structures module; The pointer will be invalid when CB_STRUCTURES_DATADESTROY is called
        structures_freqData*  (*GetFreqData)(Arena *arena, int freq); /// Get the freq data for a particular freq < STRUCTURES_FREQS
        structureData* (*GetStructureData)(Arena *arena, const char* structureID); /// Gets structure data; The pointer will be invalid when CB_STRUCTURES_DATADESTROY is called
        structureTypeData* (*GetStructureTypeData)(Arena *arena, int freq, STRUCTURETYPE type); /// Gets structure type data; The pointer will be invalid when CB_STRUCTURES_DATADESTROY is called

	void (*DoDamage)(Arena *arena, int x, int y, int damage, int radius, int immuneFreq);
} Istructures;


/** Called whenever all the data (structureData, structureTypeData, arenaData, etc) is being freed from memory. All pointers will be invalid after this */
#define CB_STRUCTURES_DATADESTROY "structures_datadestroy"
typedef void (*StructureDataDestroyFunc)(Arena *arena);

/** Called whenever a structure is spawned.*/
#define CB_STRUCTURES_STRUCTURESPAWNED "structures_structurespawned"
typedef void (*StructuresStructureSpawnedFunc)(struct structureData *sdata);

/** Called whenever a structure is destroyed */
#define CB_STRUCTURES_STRUCTUREDESTROYED "structures_structuredestroyed"
typedef void (*StructuresStructureDestroyedFunc)(struct structureData *sdata);

/** Called whenever a structure is damaged */
#define CB_STRUCTURES_STRUCTUREDAMAGED "structures_structuredamaged"
typedef void (*StructuresStructureDamagedFunc)(struct structureData *sdata, int newHealth);

/** Called whenever the structures game start in an arena */
#define CB_STRUCTURES_GAMESTART "structures_gamestart"
typedef void (*StructuresGameStartFunc)(Arena *arena);

/** Called whenever the structures game stops in an arena */
#define CB_STRUCTURES_GAMESTOP "structures_gamestop"
typedef void (*StructuresGameStopFunc)(Arena *arena);

/** Called whenever a team has won, this is NOT always called when a game stops */
#define CB_STRUCTURES_GAMEWON "structures_gamewon"
typedef void (*StructuresGameWonFunc)(Arena *arena, int freq);

/** Called whenever the structure count of a structure Type changes; NOTE: oldStructureCount MAY be -1; This means there never was a struture of this type in the game before! */
#define CB_STRUCTURES_TYPEDATASTRUCTURECOUNTCHANGED "structures_typedatastructurecountchanged"
typedef void (*StructuresTypeDataStructureCountChangedFunc)(struct structureTypeData *stdata, int oldStructureCount);

#endif
