#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "asss.h"
#include "harvest.h"
#include "cncutil.h"
#include "structures.h"
#include "attrman.h"
#include "gamecredits.h"
#include "dodamage.h"
#include "staticturret.h"

#define FLAGS (254)
#define LVZUPDATE_INTERVAL (100)
#define HARVEST_UPDATE_INTERVAL (100) //Do not set this too big or else the player may miss harvests

static Imodman		*mm;
static Iarenaman 	*aman;
static Iplayerdata	*pd;
static Ilogman		*lm;
static Imapdata		*mapdata;
static Iutil            *util;
static Iflagcore	*flagcore;
static Iconfig		*cfg;
static Iattrman		*attrman;
static Ichat		*chat;
static Imainloop	*ml;
static Inet		*net;
static Idodamage        *dodamage;
static Iprng            *prng;

static AttrmanSetter settingSetter;
static int arenaKey  = -1;
static int playerKey = -1;

struct HarvestField
{
	int oreLeft;
	ticks_t lastRecharge;
	bool visible;

	bool lvzVisible;
	ticks_t lastLVZUpdate;

	struct
	{
		Region *region;
		int capacity; //how many units can be harvested before it depletes
		int recharge; //how quick does this field regenerate (uses same recharge formula as ship recharge)
		int showFieldOn;

		int objectid;

	} config;
};

struct Refinery
{
	struct
	{
		structureData *structure;
		Region *unloadRegion;
	} config;
};

struct ArenaData
{
	Igamecredits *credits;
	Istructures *structures;
	bool running;
	ticks_t startTime;

	LinkedList fields; //struct harvestField
	LinkedList refineries; //struct Refinery

	struct
	{
		int orePerFlag;
		int safetyUnloadSpeed;
		int blowupChance;
	} config;
};

#define PlayerData PlayerData_
struct PlayerData
{
	Arena *arena;
	int harvested;
	ticks_t lastHarvest; // this value is even updated if no actual harvest was made
	ticks_t lastUnload;

	bool needsConfigReread; //config below might have changed, reread it. This is done this way so it only reads this stuff once
	                        //even though there may have been a lot of reasons to fire it
	struct
	{
		long capacity; // how many units can this player carry
		long harvestSpeed; // how quick does this player harvest; uses same formula as ship recharge
		long unloadSpeed;
	} config;
};

static void UpdateFlags(Player *p);

static struct ArenaData* GetArenaData(Arena *arena)
{
        assertlm(arena);
        struct ArenaData *adata = P_ARENA_DATA(arena, arenaKey);
        assertlm(adata);


        return adata;
}

static struct PlayerData* GetPlayerData(Player *p)
{
        assertlm(p);
        struct PlayerData *pdata = PPDATA(p, playerKey);
        assertlm(pdata);

        if (p->arena && p->arena != pdata->arena)
        {
                pdata->arena = p->arena;
        	pdata->harvested = 0;
        	pdata->lastHarvest = current_ticks();
        }
        return pdata;
}

static void SetValueCB(const Target *scope, const char *attributeName, signed char ship, AttrmanSetter setter, ParamAttrman param)
{
	LinkedList players = LL_INITIALIZER;
	Link *l;
	Player *p;
        struct PlayerData *pdata;

        pd->Lock();
        pd->TargetToSet(scope, &players);
        for (l = LLGetHead(&players); l; l = l->next)
        {
        	p = l->data;
        	if (!p || !IS_STANDARD(p)) continue;
        	pdata = GetPlayerData(p);
        	pdata->needsConfigReread = true;
        }
	pd->Unlock();
        LLEmpty(&players);

}

static void ReadPlayerSettings(Player *p)
{
	struct PlayerData *pdata;
	pdata = GetPlayerData(p);
	pdata->needsConfigReread = false;
	attrman->Lock();
	pdata->config.capacity = attrman->GetPlayerValue(p, "harvest::capacity", NULL);
	pdata->config.harvestSpeed = attrman->GetPlayerValue(p, "harvest::harvestspeed", NULL);
	pdata->config.unloadSpeed = attrman->GetPlayerValue(p, "harvest::unloadspeed", NULL);
	attrman->UnLock();

	MIN(pdata->config.capacity, 0);
	MIN(pdata->config.harvestSpeed, 0);
	MIN(pdata->config.unloadSpeed, 0);
	MAX(pdata->harvested, pdata->config.capacity);
}

static void StartGame(Arena *arena)
{
	struct ArenaData *adata;
	struct PlayerData *pdata;
	struct HarvestField *harvestField;
	Player *p;
	Link *link;

	adata = GetArenaData(arena);
	if (adata->running) return;
	adata->running = true;
	adata->startTime = current_ticks();

	for (link = LLGetHead(&adata->fields); link; link = link->next)
	{
		harvestField = (struct HarvestField *) link->data;
		harvestField->oreLeft = harvestField->config.capacity; // MainLoopCB will do the lvz
	}

	FOR_EACH_PLAYER(p)
	{
		if (!IS_STANDARD(p)) continue;
		if (p->arena != arena) continue;
		pdata = GetPlayerData(p);

		pdata->harvested = 0;
		pdata->needsConfigReread = true;
	}
}

static void StopGame(Arena *arena)
{
	struct ArenaData *adata;
	struct PlayerData *pdata;
	Link *link;
	Player *p;

	adata = GetArenaData(arena);
	if (!adata->running) return;
	adata->running = false;

	FOR_EACH_PLAYER(p)
	{
		if (!IS_STANDARD(p)) continue;
		if (p->arena != arena) continue;
		pdata = GetPlayerData(p);

		pdata->harvested = 0;
		UpdateFlags(p); //game has stopped, so the mainloop does not update it anymore
	}
}

static void ReadSettings(Arena *arena)
{
	struct ArenaData *adata;
	ConfigHandle ch;
	Target tgt;
	int a;
	char buf[256];
	const char *s;
	struct HarvestField *harvestField;
	struct Refinery *refinery;
	Region *region;
	ticks_t now;
	structureData *structure;

	now = current_ticks();
	adata = GetArenaData(arena);
	ch = arena->cfg;
	tgt.type = T_ARENA; tgt.u.arena = arena;

	LLInit(&adata->fields);
	LLInit(&adata->refineries);

	adata->config.orePerFlag = cfg->GetInt(ch, "harvest", "OrePerFlag", 100);
	adata->config.blowupChance = cfg->GetInt(ch, "harvest", "ExplosionChance", 0); // rand() < (harvested / (HarvestCapacity * (ExplosionChance / 1000.0)))
	MIN(adata->config.orePerFlag, 1);
	adata->config.safetyUnloadSpeed = cfg->GetInt(ch, "harvest", "SafetyUnloadSpeed", 200);

	attrman->Lock();
	for (a = 0; a < 8; a++)
	{
		attrman->SetValue(&tgt, settingSetter, "harvest::capacity", true, a,
		                  cfg->GetInt(ch, util->GetShipName(a), "HarvestCapacity", 0));

		attrman->SetValue(&tgt, settingSetter, "harvest::harvestspeed", true, a,
		                  cfg->GetInt(ch, util->GetShipName(a), "HarvestSpeed", 0));

		attrman->SetValue(&tgt, settingSetter, "harvest::unloadspeed", true, a,
		                  cfg->GetInt(ch, util->GetShipName(a), "HarvestUnloadSpeed", 0));
	}
	attrman->UnLock();

	for (a = 0; a < 100; a++)
	{
		snprintf(buf, sizeof(buf), "Field%d", a);
		s = cfg->GetStr(ch, "harvest", buf);
		if (!s) break;

		snprintf(buf, sizeof(buf), "harvest_field_%s", s);
		s = cfg->GetStr(ch, buf, "region");
		if (!s)
		{
			lm->LogA(L_ERROR, "harvest", arena, "Missing setting %s:region", buf);
			continue;
		}

		region = mapdata->FindRegionByName(arena, s);
		if (!region)
		{
			lm->LogA(L_ERROR, "harvest", arena, "Unknown region %s for setting %s:region", s, buf);
			continue;
		}

		harvestField = amalloc(sizeof(struct HarvestField));
		harvestField->config.region = region;
		harvestField->config.capacity = cfg->GetInt(ch, buf, "capacity", 1000);
		harvestField->config.recharge = cfg->GetInt(ch, buf, "recharge", 100);
		harvestField->config.objectid = cfg->GetInt(ch, buf, "objectid", -1);
		harvestField->config.showFieldOn = cfg->GetInt(ch, buf, "ShowFieldOn", 250);
		MIN(harvestField->config.showFieldOn, 1);

		harvestField->oreLeft = harvestField->config.capacity;
		harvestField->lastRecharge = TICK_MAKE(now + a);
		harvestField->visible = false; // will be turned "on" on game start

		LLAdd(&adata->fields, harvestField);
		lm->LogA(L_DRIVEL, "harvest", arena, "Added field %s", buf);
	}

	for (a = 0; a < 100; a++)
	{
		snprintf(buf, sizeof(buf), "Refinery%d", a);
		s = cfg->GetStr(ch, "harvest", buf);
		if (!s) break;

		snprintf(buf, sizeof(buf), "harvest_refinery_%s", s);
		s = cfg->GetStr(ch, buf, "Structure");
		if (!s)
		{
			lm->LogA(L_ERROR, "harvest", arena, "Missing setting %s:structure", buf);
			continue;
		}

		structure = adata->structures->GetStructureData(arena, s);
		if (!structure)
		{
			lm->LogA(L_ERROR, "harvest", arena, "Unknown structure %s for setting %s:structure", s, buf);
			continue;
		}

		s = cfg->GetStr(ch, buf, "region");
		if (!s)
		{
			lm->LogA(L_ERROR, "harvest", arena, "Missing setting %s:region", buf);
			continue;
		}

		region = mapdata->FindRegionByName(arena, s);
		if (!region)
		{
			lm->LogA(L_ERROR, "harvest", arena, "Unknown region %s for setting %s:region", s, buf);
			continue;
		}

		refinery = amalloc(sizeof(struct Refinery));
		refinery->config.structure = structure;
		refinery->config.unloadRegion = region;
		LLAdd(&adata->refineries, refinery);
	}
}

static void CleanupArena(Arena *arena)
{
	struct ArenaData *adata;
	Link *l;

	adata = GetArenaData(arena);

	for (l = LLGetHead(&adata->fields); l; l = l->next)
	{
		afree(l->data);
	}
	LLEmpty(&adata->fields);

	for (l = LLGetHead(&adata->refineries); l; l = l->next)
	{
		afree(l->data);
	}
	LLEmpty(&adata->refineries);
}

static void StructuresDataDestroyCB(Arena *arena)
{
	//todo: remove the pointers to any structure
}

// reset the harvest without removing flags; this is just a quick function to use in callbacks
static void ResetHarvest(Player *p)
{
	struct PlayerData *pdata;
	pdata = GetPlayerData(p);
	pdata->harvested = 0;
}

static void RemoveFlag(Arena *arena, int fid)
{
	FlagInfo fi;

	fi.state = FI_NONE;
	fi.carrier = NULL;
	fi.x = 0;
	fi.y = 0;
	fi.freq = -1;
	flagcore->SetFlags(arena, fid, &fi, 1);
}

static int GiveFlags(Player *p, int count)
{
	Arena *arena;
	int fid;
	FlagInfo fi;
	int given;

	arena = p->arena;
	if (count <= 0) return 0;
	if (!arena) return 0;

	for (fid = 0, given = 0; fid < FLAGS; fid++)
	{
		flagcore->GetFlags(arena, fid, &fi, 1);
		if (fi.state == FI_NONE)
		{
			fi.state = FI_CARRIED;
			fi.carrier = p;
			fi.freq = p->p_freq;
			flagcore->SetFlags(arena, fid, &fi, 1);
			given++;

			if (given >= count)
				return given; // done
		}
	}
	return 0;
}

static int RemoveFlags(Player *p, int count)
{
	Arena *arena;
	int fid;
	FlagInfo fi;
	int removed;

	arena = p->arena;
	if (count <= 0) return 0;
	if (!arena) return 0;

	for (fid = 0, removed = 0; fid < FLAGS; fid++)
	{
		flagcore->GetFlags(arena, fid, &fi, 1);
		if (fi.state == FI_CARRIED && fi.carrier == p)
		{
			RemoveFlag(arena, fid);
			removed++;

			if (removed >= count)
				return removed; // done
		}
	}
	return 0;
}

static int SetFlags(Player *p, int count)
{
	int oldCount;

	if (!p->arena) return 0;
	oldCount = flagcore->CountPlayerFlags(p);

	if (count > oldCount)
		return oldCount + GiveFlags(p, count - oldCount);
	else if (count < oldCount)
		return oldCount - RemoveFlags(p, oldCount - count);
	else //count == oldCount
		return count;

}

static void UpdateFlags(Player *p)
{
	struct PlayerData *pdata;
	struct ArenaData *adata;

	pdata = GetPlayerData(p);
	adata = GetArenaData(p->arena);

	MAX(pdata->harvested, pdata->config.capacity);
	MIN(adata->config.orePerFlag, 1);
	SetFlags(p, pdata->harvested / adata->config.orePerFlag);
}

static void MineOre(Arena *arena, Player *miner, int freq, int ore)
{
	struct ArenaData *adata;
	adata = GetArenaData(arena);

	if (adata->credits)
		adata->credits->MineOre(arena, miner, freq, ore);
}


static void MainLoopCB()
{
	struct ArenaData *adata;
	struct PlayerData *pdata;
	struct HarvestField *harvestField;
	struct Refinery *refinery;
	Player *p;
	Link *link, *l;
	ticks_t now;
	Arena *arena;
	int diff, harvest, remaining, unload;

	now = current_ticks();
	// this function does 3 things:
	// 1. Let players harvest the fields
	// 2. Let players unload their ore into a refinery
	// 3. Recharge the ore fields

	// harvest the fields
	pd->Lock();
	FOR_EACH_PLAYER(p)
	{
		if (!IS_STANDARD(p)) continue;
		arena = p->arena;
		if (!arena) continue;
		adata = GetArenaData(arena);
		pdata = GetPlayerData(p);

		if (!adata->running)
		{
			continue;
		}

		if (pdata->needsConfigReread)
			ReadPlayerSettings(p);

		if (!pdata->lastHarvest || TICK_GT(adata->startTime, pdata->lastHarvest)) // invalid time
			pdata->lastHarvest = now;

		diff = TICK_DIFF(now, pdata->lastHarvest);
		if (diff >= HARVEST_UPDATE_INTERVAL)
		{
			harvest = (diff * pdata->config.harvestSpeed) / 1000;
			if (harvest)
				pdata->lastHarvest = now;

			if (harvest && adata->running && pdata->config.capacity > 0 && pdata->harvested < pdata->config.capacity)
			{
				remaining = harvest;

				for (l = LLGetHead(&adata->fields); l; l = l->next)
				{
					harvestField = (struct HarvestField *) l->data;
					if (harvestField->oreLeft && harvestField->visible &&
					    mapdata->Contains(harvestField->config.region, p->position.x >> 4, p->position.y >> 4))
					{
						if (remaining < harvestField->oreLeft) // the field has plenty of units left
						{
							//lm->LogA(L_DRIVEL, "harvest", arena, "%d -= %d", harvestField->oreLeft, remaining);
							harvestField->oreLeft -= remaining;
							remaining = 0;

						}
						else // the field is almost empty, now empty it completely
						{
							remaining -= harvestField->oreLeft;
							harvestField->oreLeft = 0;
						}

						if (remaining <= 0) break; // no need to look any further

					}
				}

				pdata->harvested += harvest - remaining;


			}
		}

		if (!pdata->lastUnload || TICK_GT(adata->startTime, pdata->lastUnload)) // invalid time
			pdata->lastUnload = now;

		//unload the ore into a refinery
		diff = TICK_DIFF(now, pdata->lastUnload);
		if (diff >= HARVEST_UPDATE_INTERVAL)
		{
			unload = (diff * pdata->config.unloadSpeed) / 1000;

			if (unload)
				pdata->lastUnload = now;

			if (unload && adata->running && pdata->harvested > 0)
			{
				for (l = LLGetHead(&adata->refineries); l; l = l->next)
				{
					refinery = (struct Refinery *) l->data;
					if (refinery->config.structure->alive && mapdata->Contains(refinery->config.unloadRegion, p->position.x >> 4, p->position.y >> 4))
					{
						if (pdata->harvested > unload) // the unload speed is not big enough to unload everything right now
						{
							MineOre(arena, p, refinery->config.structure->config.freq, unload);
							pdata->harvested -= unload;
						}
						else // there is not much left in his cargo, empty it
						{
							MineOre(arena, p, refinery->config.structure->config.freq, pdata->harvested);
							pdata->harvested = 0;
						}
						break; // no need to look further
					}

				}

				if (adata->config.safetyUnloadSpeed && p->position.status & STATUS_SAFEZONE)
				{
					pdata->harvested -= (diff * adata->config.safetyUnloadSpeed) / 1000;
					MIN(pdata->harvested, 0);
				}
			}
		}
		UpdateFlags(p);
	}
	pd->Unlock();

	pdata = NULL;

	aman->Lock();
	FOR_EACH_ARENA(arena)
	{
		adata = GetArenaData(arena);

		if (!adata->running)
		{
			continue;
		}

		// recharge the fields;
		for (l = LLGetHead(&adata->fields); l; l = l->next)
		{
			harvestField = (struct HarvestField *) l->data;

			if (!harvestField->lastRecharge || TICK_GT(adata->startTime, harvestField->lastRecharge)) // invalid time
				harvestField->lastRecharge = now;

			if (harvestField->oreLeft < harvestField->config.capacity)
			{
				diff = (harvestField->config.recharge * TICK_DIFF(now, harvestField->lastRecharge)) / 1000;
				if (diff)
				{
					harvestField->oreLeft += diff;
					harvestField->lastRecharge = now;
					//lm->LogA(L_DRIVEL, "harvest", arena, "Ore left: %d; Previous Visible: %d", harvestField->oreLeft, harvestField->visible);
				}
			}

			if (harvestField->oreLeft >= harvestField->config.showFieldOn)
				harvestField->visible = true;
			else if (harvestField->oreLeft <= 0)
				harvestField->visible = false;

			MAX(harvestField->oreLeft, harvestField->config.capacity);


			if (harvestField->config.objectid >= 0 &&
			    harvestField->visible != harvestField->lvzVisible &&
			    TICK_DIFF(now, harvestField->lastLVZUpdate) > LVZUPDATE_INTERVAL)
			{
				util->ObjToggle(arena, harvestField->config.objectid, harvestField->visible ? 1 : 0, true);
				harvestField->lvzVisible = harvestField->visible;
				harvestField->lastLVZUpdate = now;
			}
		}
	}

	aman->Unlock();
}

static void PlayerActionCB(Player *p, int action, Arena *arena)
{
	struct PlayerData *pdata;
	pdata = GetPlayerData(p);
	if (action == PA_ENTERGAME)
	{
		pdata->needsConfigReread = true;
	}
	else if (action == PA_LEAVEARENA)
	{
	}
}

static void ShipFreqChangeCB(Player *p, int newship, int oldship, int newfreq, int oldfreq)
{
	struct PlayerData *pdata;
	pdata = GetPlayerData(p);
	pdata->needsConfigReread = true;
	ResetHarvest(p);
}

static void SpawnCB(Player *p, int reason)
{
	ResetHarvest(p);
}

static void AttachCB(Player *p, Player *to)
{
	ResetHarvest(p);
}

static void KillCB(Arena *arena, Player *killer, Player *killed, int bounty, int flags, int pts, int green)
{
	struct PlayerData *pdata;
	struct ArenaData *adata;
	double chance;
	pdata = GetPlayerData(killed);
	adata = GetArenaData(killed->arena);
	Target victims;
	int turretdamage, turretradius;
	int structuredamage, structureradius;

	if (pdata->harvested)
	{
		chance = (pdata->config.capacity * (adata->config.blowupChance / 1000.0));

		if (chance)
		{
			chance = pdata->harvested / chance;

			if (prng->Uniform() < chance) // boom
			{
				victims.type = T_ARENA;
				victims.u.arena = arena;

				turretdamage = cfg->GetInt(arena->cfg, "harvest", "ExplosionTurretDamage", 0);
				turretradius = cfg->GetInt(arena->cfg, "harvest", "ExplosionTurretRadius", 0);
				structuredamage = cfg->GetInt(arena->cfg, "harvest", "ExplosionStructureDamage", 0);
				structureradius = cfg->GetInt(arena->cfg, "harvest", "ExplosionStructureRadius", 0);

				// "Tiberium Explosion" = wrong grammar
				Player *killer = dodamage->MapDamageN(arena, &victims, "<Tiberian Explosion>", killed->position.x, killed->position.y, W_THOR, 3, 31, 3, true);
				Istaticturret *turret = mm->GetInterface(I_STATICTURRET, arena);
				if (turret)
					turret->DoDamage(arena, killer,  killed->position.x, killed->position.y, turretdamage, turretradius, -1);
				mm->ReleaseInterface(turret);

				Istructures *structures = mm->GetInterface(I_STRUCTURES, arena);
				if (structures)
					structures->DoDamage(arena, killed->position.x, killed->position.y, structuredamage, structureradius, -1);
				mm->ReleaseInterface(structures);
			}

		}
	}

	ResetHarvest(killed);
}

static void FlagGainCB(Arena *arena, Player *p, int fid, int how)
{
	if (how != FLAGGAIN_OTHER)
		RemoveFlag(arena, fid);
}

static void FlagLostCB(Arena *arena, Player *p, int fid, int how)
{
	if (how != CLEANUP_OTHER)
		RemoveFlag(arena, fid);
}

static void FlagOnMapCB(Arena *arena, int fid, int x, int y, int freq)
{
	RemoveFlag(arena, fid);
}

static void FlagResetCB(Arena *arena, int freq, int points)
{
	Player *p;
	Link *link;
	struct ArenaData *adata;

	if (!arena) return;
	adata = GetArenaData(arena);
	if (!adata->running) return;
	// give the flags back

	pd->Lock();
	FOR_EACH_PLAYER(p)
	{
		if (p->arena == arena && (freq < 0 || freq == p->p_freq))
		{
			UpdateFlags(p);
		}
	}
	pd->Unlock();
}

static void PlayerWarpCB(Player *p)
{
	struct ArenaData *adata;

	if (!p->arena) return;
	adata = GetArenaData(p->arena);

	if (adata->running)
		ResetHarvest(p);
}

static int pppkPlayerKey = -1;
struct pppkPlayerData
{
	int oldX;
	int oldY;
	u32 lastPositionPacket;
};

// partially adapted from mervbot's ctf2 plugin
static void Pppk(Player *p, byte *pkt, int len) // not ran in the main thread!
{
	struct C2SPosition *pos;
	struct pppkPlayerData *pdata;
	Arena *arena;

	pos = (struct C2SPosition *)pkt;
	arena = p->arena;
	pdata = PPDATA(p, pppkPlayerKey);

	if (TICK_DIFF(pos->time,pdata->lastPositionPacket) < 0) return; // old position packet, ignore it
	pdata->lastPositionPacket = pos->time;

	if (arena && p->p_ship >= SHIP_WARBIRD && p->p_ship <= SHIP_SHARK &&
		pos->x != -1 && pos->y != -1)
	{

		if (pos->status & STATUS_FLASH)
		{
			int dx = (pdata->oldX - pos->x) >> 4;
			int dy = (pdata->oldY - pos->y) >> 4;

			if (dx*dx + dy*dy >= 20*20)
			{
				ml->SetTimer((TimerFunc) PlayerWarpCB, 0, 0, p, NULL);
			}
		}

		pdata->oldX = pos->x;
		pdata->oldY = pos->y;

	}


}

static void FlagGameInit(Arena *arena)
{
	flagcore->SetCarryMode(arena, CARRY_ALL);
	flagcore->ReserveFlags(arena, FLAGS);
}

static void FlagGameFlagTouch(Arena *arena, Player *p, int fid)
{
	// no need to do anything here
}

static void FlagGameCleanup(Arena *arena, int fid, int reason, Player *oldcarrier, int oldfreq)
{
	// no need to do anything here
}


static Iharvest harvest =
{
	INTERFACE_HEAD_INIT(I_HARVEST, "harvest")
	StartGame, StopGame
};

static Iflaggame flaggame =
{
	INTERFACE_HEAD_INIT(I_FLAGGAME, "harvestfg")
	FlagGameInit,
	FlagGameFlagTouch,
	FlagGameCleanup
};

EXPORT const char info_harvest[] = "Harvest $ by JoWie ("BUILDDATE")\n";
static void ReleaseInterfaces()
{
        mm->ReleaseInterface(aman    );
        mm->ReleaseInterface(pd      );
        mm->ReleaseInterface(util    );
        mm->ReleaseInterface(lm      );
        mm->ReleaseInterface(mapdata );
        mm->ReleaseInterface(flagcore);
        mm->ReleaseInterface(attrman );
        mm->ReleaseInterface(chat    );
        mm->ReleaseInterface(cfg     );
        mm->ReleaseInterface(ml      );
        mm->ReleaseInterface(net     );
        mm->ReleaseInterface(dodamage);
        mm->ReleaseInterface(prng    );
}

EXPORT int MM_harvest(int action, Imodman *mm_, Arena *arena)
{
	struct ArenaData *adata;

        if (action == MM_LOAD)
        {
                mm       = mm_;
                aman     = mm->GetInterface(I_ARENAMAN        , ALLARENAS);
                pd       = mm->GetInterface(I_PLAYERDATA      , ALLARENAS);
                util     = mm->GetInterface(I_UTIL            , ALLARENAS);
                lm       = mm->GetInterface(I_LOGMAN          , ALLARENAS);
		mapdata  = mm->GetInterface(I_MAPDATA         , ALLARENAS);
		flagcore = mm->GetInterface(I_FLAGCORE        , ALLARENAS);
		cfg      = mm->GetInterface(I_CONFIG          , ALLARENAS);
		attrman	 = mm->GetInterface(I_ATTRMAN         , ALLARENAS);
		chat     = mm->GetInterface(I_CHAT            , ALLARENAS);
		ml       = mm->GetInterface(I_MAINLOOP        , ALLARENAS);
		net      = mm->GetInterface(I_NET             , ALLARENAS);
		dodamage = mm->GetInterface(I_DODAMAGE        , ALLARENAS);
		prng     = mm->GetInterface(I_PRNG            , ALLARENAS);

                if (!aman || !pd || !util || !lm || !mapdata || !flagcore || !cfg || !attrman || !chat || !ml || !net || !dodamage || !prng)
                {
                        ReleaseInterfaces();
                        printf("<harvest> Missing interfaces");
                        return MM_FAIL;
                }

                arenaKey = aman->AllocateArenaData(sizeof(struct ArenaData));
                playerKey = pd->AllocatePlayerData(sizeof(struct PlayerData));
                pppkPlayerKey = pd->AllocatePlayerData(sizeof(struct pppkPlayerData));


                if (arenaKey == -1 || playerKey == -1 || pppkPlayerKey == -1) // check if we ran out of memory
                {
                        if (arenaKey  != -1) // free data if it was allocated
                                aman->FreeArenaData(arenaKey);

                        if (playerKey != -1) // free data if it was allocated
                                pd->FreePlayerData(playerKey);

                        if (pppkPlayerKey != -1)
                        	pd->FreePlayerData(pppkPlayerKey);

                        ReleaseInterfaces();

                        return MM_FAIL;
                }

		attrman->Lock();
		settingSetter = attrman->RegisterSetter();
		attrman->RegisterCallback("harvest::capacity", SetValueCB, ATTRMAN_NO_PARAM);
		attrman->RegisterCallback("harvest::harvestspeed", SetValueCB, ATTRMAN_NO_PARAM);
		attrman->RegisterCallback("harvest::unloadspeed", SetValueCB, ATTRMAN_NO_PARAM);
		attrman->UnLock();

		mm->RegCallback(CB_MAINLOOP, MainLoopCB, ALLARENAS);
		net->AddPacket(C2S_POSITION, Pppk); //needed to track warping

		

                return MM_OK;

        }
        else if (action == MM_UNLOAD)
        {
        	if (harvest.head.arena_registrations)
        		return MM_FAIL;

		net->RemovePacket(C2S_POSITION, Pppk);
		mm->UnregCallback(CB_MAINLOOP, MainLoopCB, ALLARENAS);

		attrman->Lock();
		aman->FreeArenaData(arenaKey);
                pd->FreePlayerData(playerKey);
                pd->FreePlayerData(pppkPlayerKey);
		
		attrman->UnregisterCallback("harvest::capacity", SetValueCB);
		attrman->UnregisterCallback("harvest::harvestspeed", SetValueCB);
		attrman->UnregisterCallback("harvest::unloadspeed", SetValueCB);
		attrman->UnregisterSetter(settingSetter);
		attrman->UnLock();

                ReleaseInterfaces();

                return MM_OK;
        }
        else if (action == MM_ATTACH)
        {
        	adata = GetArenaData(arena);
        	adata->credits = mm->GetInterface(I_GAMECREDITS, arena);
        	adata->structures = mm->GetInterface(I_STRUCTURES, arena);

        	if (!adata->credits || !adata->structures)
        	{
        		lm->LogA(L_ERROR, "harvest", arena, "Unable to attach, missing interface (wrong attach order?)");
        		mm->ReleaseInterface(adata->credits);
        		mm->ReleaseInterface(adata->structures);
        		return MM_FAIL;
        	}

		mm->RegInterface(&harvest, arena);
		mm->RegInterface(&flaggame, arena);

		mm->RegCallback(CB_MAINLOOP, MainLoopCB, arena);
		mm->RegCallback(CB_PLAYERACTION, PlayerActionCB, arena);
		mm->RegCallback(CB_SHIPFREQCHANGE, ShipFreqChangeCB, arena);
		mm->RegCallback(CB_KILL, KillCB, arena);
		mm->RegCallback(CB_SPAWN, SpawnCB, arena);
		mm->RegCallback(CB_FLAGGAIN, FlagGainCB, arena);
		mm->RegCallback(CB_FLAGLOST, FlagLostCB, arena);
		mm->RegCallback(CB_FLAGONMAP, FlagOnMapCB, arena);
		mm->RegCallback(CB_FLAGRESET, FlagResetCB, arena);
		mm->RegCallback(CB_STRUCTURES_DATADESTROY, StructuresDataDestroyCB, arena);
		mm->RegCallback(CB_ATTACH, AttachCB, arena);

		ReadSettings(arena);

                return MM_OK;
        }
        else if (action == MM_DETACH)
        {
                adata = GetArenaData(arena);
		if (mm->UnregInterface(&harvest, arena))
			return MM_FAIL;

		if (mm->UnregInterface(&flaggame, arena))
		{
			mm->RegInterface(&harvest, arena);
			return MM_FAIL;
		}
		mm->ReleaseInterface(adata->credits);
		mm->ReleaseInterface(adata->structures);

		mm->UnregCallback(CB_MAINLOOP, MainLoopCB, arena);
		mm->UnregCallback(CB_PLAYERACTION, PlayerActionCB, arena);
		mm->UnregCallback(CB_SHIPFREQCHANGE, ShipFreqChangeCB, arena);
		mm->UnregCallback(CB_KILL, KillCB, arena);
		mm->UnregCallback(CB_SPAWN, SpawnCB, arena);
		mm->UnregCallback(CB_FLAGGAIN, FlagGainCB, arena);
		mm->UnregCallback(CB_FLAGLOST, FlagLostCB, arena);
		mm->UnregCallback(CB_FLAGONMAP, FlagOnMapCB, arena);
		mm->UnregCallback(CB_FLAGRESET, FlagResetCB, arena);
		mm->UnregCallback(CB_STRUCTURES_DATADESTROY, StructuresDataDestroyCB, arena);
		mm->UnregCallback(CB_ATTACH, AttachCB, arena);

		StopGame(arena);
		CleanupArena(arena);

                return MM_OK;
        }

        return MM_FAIL;
}
