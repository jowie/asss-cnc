#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#include "cncutil.h"
#include "asss.h"
#include "objects.h"
#include "damage.h"
#include "staticturret.h"
#include "jackpot.h"
#include "swappablegame.h"
#include "gamecredits.h"
#include "advbuy.h"
#include "structures.h"
#include "harvest.h"
#include "attrman.h"

#define MODULE "structures"

#define arenaData structures_arenaData
#define freqData structures_freqData

/*
 * structures v2 by JoWie (rewritten from structures v1 by smong)
 *
 * Destructible structures you can add to your map. once one team has lost all
 * their structures the game ends.
 *
 * commands:
 *
 * ?structuresoptimalization
 *
 * settings:


    todo: more info

*/

static Imodman       *mm;
static Ilogman       *lm;
static Iconfig       *cfg;
static Imainloop     *ml;
static Iarenaman     *aman;
static Idamage       *damage;
static Imapdata      *mapdata;
static Icmdman       *cmd;
static Iobjects      *objs;
static Ichat         *chat;
static Iplayerdata   *pd;
static Istats        *stats;
static Igame         *game;
static Ijackpot      *jackpot;
static Iutil         *util;
static Iattrman      *attrman;

static int arenaKey = -1;
static AttrmanSetter settingSetter;

static void                            ReadConfig                                (Arena *arena);
static void                            InitializeArena                           (Arena *arena);
static void                            CleanupArena                              (Arena *arena);
static void                            StartGame                                 (Arena *arena);
static void                            StopGame                                  (Arena *arena);
static void                            SwapGameStartGame                         (Arena *arena, GameoverFunc gameOverFunc);
static void                            SwapGameStopGame                          (Arena *arena);
static void                            RewardPoints                              (Arena *arena, int freq);
static void                            DestroyStructure                          (struct structureData *sdata, bool silent);
static void                            DestroyStructureInterface                 (const struct structureData *sdata, bool silent);
static void                            SpawnStructure                            (struct structureData *sdata, bool silent);
static void                            SpawnStructureInterface                   (const struct structureData *sdata, bool silent);
static int                             SetHealth                                 (struct structureData *sdata, int health, bool silent);
static int                             SetHealthInterface                        (const struct structureData *sdata, int health, bool silent);
static void                            UpdateStructureTypes                      (struct structureData *sdata);
static void                            UpdateStructureFx                         (struct structureData *sdata, bool silent);
static void                            UpdatePercentageFx                        (Arena *arena);
static void                            StructureTileDamage                       (Arena *arena, int x, int y, Player *firedBy, int damage, int wtype, int level, bool bouncingbomb, int emptime, void *clos);
static int                             StructureRefreshTimer                     (void *arena_);
//static void                            ArenaAction                               (Arena *arena, int action);
static void                            Cstructuresoptimalization                 (const char *tc, const char *params, Player *p, const Target *target);
static bool                            Cstructuresoptimalization_checkCoordsMap  (Player *p, const char *typeID, int objectid, int configX, int configY);
static struct arenaData*               GetArenaData                              (Arena *arena);
static struct freqData*                GetFreqData                               (Arena *arena, int freq);
static struct structureData*           GetStructureData                          (Arena *arena, const char* typeID);
static struct structureTypeData*       GetStructureTypeData                      (Arena *arena, int freq, STRUCTURETYPE type);
static const struct arenaData*         GetArenaDataInterface                     (Arena *arena);
static const struct freqData*          GetFreqDataInterface                      (Arena *arena, int freq);
static const struct structureData*     GetStructureDataInterface                 (Arena *arena, const char* typeID);
static const struct structureTypeData* GetStructureTypeDataInterface             (Arena *arena, int freq, STRUCTURETYPE type);
static void                            SetRespawnRate                            (Arena *arena, int freq, unsigned int value);
static void                            SetRepairRate                             (Arena *arena, int freq, unsigned int value);
static void                            DoDamage                                  (Arena *arena, int x, int y, int damage, int radius, int immuneFreq);
static void                            ReleaseInterfaces                         ();
EXPORT int                             MM_structures                             (int action, Imodman *mm_, Arena *arena);

static Istructures structuresInterface =
{
        INTERFACE_HEAD_INIT(I_STRUCTURES, "structures")

        SetRespawnRate,
        SetRepairRate,

        SetHealthInterface,
        SpawnStructureInterface,
        DestroyStructureInterface,

        GetArenaDataInterface,
        GetFreqDataInterface,
        GetStructureDataInterface,
        GetStructureTypeDataInterface,
        DoDamage
};

static Iswappablegame swapgameInterface =
{
        INTERFACE_HEAD_INIT(I_SWAPPABLEGAME_STRUCTURES, "sgame-structures")

        SwapGameStartGame,
        SwapGameStopGame
};

#define tFREE(a) \
if (a) \
{ \
        afree(a); \
        a = NULL; \
} \

struct arenaDataContainer
{
        struct arenaData *realArenaData; //lets not waste memory on non attached arenas
};

static const char * const structureTypes_settingNameIs[STRUCTURES_STRUCTURETYPES] =
{
        "IsConstructionYard",
        "IsPowerPlant",
        "IsOreRefinery",
        "IsBarracks",
        "IsWarfactory",
        "IsAirfield",
        "IsRadar",
        "IsTech",
        "IsSilo",
        "IsRepair",
        "IsNuke",
        "IsSecondSuperWeapon"
};
static const char * const structureTypes_settingName[STRUCTURES_STRUCTURETYPES] =
{
        "ConstructionYard",
        "PowerPlant",
        "OreRefinery",
        "Barracks",
        "Warfactory",
        "Airfield",
        "Radar",
        "Tech",
        "Silo",
        "Repair",
        "Nuke",
        "SecondSuperWeapon"
};

static helptext_t Cstructuresoptimalization_help =
        "Targets: none\n"
        "Args: <maximum results>\n"
        "Shows possible optimalizations that can be made in the lvz files of structures\n"
        "This command also looks for some errors that can be made in the config dealing with lvz";

static void SetValueCB(const Target *scope, const char *attributeName, signed char ship, AttrmanSetter setter, ParamAttrman param)
{
	Target tgtFreq;
	struct structureData *sdata;
	struct freqData *fdata;
	struct structureTypeData *stdata;
	long old, *val, a;
	Arena *arena;

	tgtFreq.type = T_FREQ;
	sdata = param.pointer;
	arena = sdata->arena;

	tgtFreq.u.freq.arena = sdata->arena;
	tgtFreq.u.freq.freq = sdata->config.freq;

	val = (int*) param.pointer2;
	old = *val;
	attrman->Lock();
	*val = attrman->GetTotalValue(&tgtFreq, attributeName, ATTRMAN_NO_SHIP, NULL);
	attrman->UnLock();	

	if (param.pointer2 == &sdata->config.initialHealth)
	{
		MIN(sdata->config.initialHealth, 1);

		// update the health
		if (sdata->config.freq < STRUCTURES_FREQS && sdata->config.isBase)
		{
		        fdata = GetFreqData(arena, sdata->config.freq);
		        fdata->structureTotalInitialHealth += sdata->config.initialHealth - old;
		}

		for (a = 0; a < STRUCTURES_STRUCTURETYPES; a++)
                {
                        if (sdata->config.structureType & 1 << a)
                        {
                                stdata = GetStructureTypeData(arena, sdata->config.freq, a);
                                stdata->structureCount++;

                                stdata->structureTotalInitialHealth += sdata->config.initialHealth - old;
                        }
                }
	}
	else if (param.pointer2 == &sdata->config.respawnTicks)
	{
		if (sdata->config.respawnTicks < 0)
			sdata->config.respawnTicks = 0;
		else
			sdata->config.respawnTicks *= 100;

		sdata->respawnTicksLeft += sdata->config.respawnTicks - old;
	}
	else if (param.pointer2 == &sdata->config.rewardBonus)
	{
		MIN(sdata->config.rewardBonus, 0);

		//update the reward
		if (sdata->alive && sdata->config.freq < STRUCTURES_FREQS)
		{
		        fdata = GetFreqData(arena, sdata->config.freq);
		        fdata->rewardBonus += sdata->config.rewardBonus - old;
		}
	}

	if (sdata->config.repairLT100 < 0) sdata->config.repairLT100 = 0;
        if (sdata->config.repairLT50 < 0) sdata->config.repairLT50 = sdata->config.repairLT100;
        if (sdata->config.repairLT25 < 0) sdata->config.repairLT25 = sdata->config.repairLT50;
}

/** Read the configuration from arena settings; Also sets some related variables to their initial values */
static void ReadConfig(Arena *arena)
{
        assertlm(arena);
        struct arenaData *ad = GetArenaData(arena);
        assertlm(!ad->running && !ad->initialized); // This function can only be called if the arena has not been initialized yet

        ConfigHandle ch = arena->cfg;
        struct structureData *sdata;
        struct freqData *fdata;
        Target tgtFreq;
        tgtFreq.type = T_FREQ;
        tgtFreq.u.freq.arena = arena;

        const char *tmpS;
        char buf[128], buf2[128];
        int a, b;
        int tmpInt;
        ParamAttrman param;

        bool usehpbar = true;


        ad->structures = HashAlloc();

        ad->tgt.type = T_ARENA;
        ad->tgt.u.arena = arena;

        ad->config.hideScreenObjectsOnGameStop = !!cfg->GetInt(ch, "structures", "HideScreenObjectsOnGameStop", true);

        ad->config.extraReward = cfg->GetInt(ch, "structures", "ExtraReward", 0);
        ad->config.reward = cfg->GetInt(ch, "structures", "Reward", 0);
        ad->config.useJackpot = !!cfg->GetInt(ch, "structures", "UseJackpot", false);
        ad->config.splitPoints = !!cfg->GetInt(ch, "structures", "SplitPoints", false);
        ad->config.weightedDamage = !!cfg->GetInt(ch, "structures", "WeightedDamage", false);

        ad->config.gameRunningObjectid = cfg->GetInt(ch, "structures", "GameRunningObjectid", -1);

        // read hp bar settings
        ad->config.hpbar_objectstart = cfg->GetInt(ch, "structures_hpbar", "ObjectStart", -1);
        ad->config.hpbar_objectsallocated = cfg->GetInt(ch, "structures_hpbar", "ObjectsAllocated", 0);
        ad->config.hpbar_imagestart = cfg->GetInt(ch, "structures_hpbar", "ImageStart", -1);
        ad->config.hpbar_imagesallocated = cfg->GetInt(ch, "structures_hpbar", "ImagesAllocated", 0);
        ad->config.hpbar_xoffset = cfg->GetInt(ch, "structures_hpbar", "XOffset", 0);
        ad->config.hpbar_yoffset = cfg->GetInt(ch, "structures_hpbar", "YOffset", 0);
        ad->hpbar_objectTicker = 0;


        MIN(ad->config.gameRunningObjectid ,-1);
        MIN(ad->config.hpbar_objectstart,-1);
        MIN(ad->config.hpbar_objectsallocated,0);
        MIN(ad->config.hpbar_imagestart,0);
        MIN(ad->config.hpbar_imagesallocated,0);


        // hpbar checks
        if (ad->config.hpbar_objectsallocated <= 0 || ad->config.hpbar_objectstart < 0)
        {
                lm->LogA(L_WARN, "structures", arena, "hpbar: no objects allocated");
                usehpbar = false;
        }

        if (ad->config.hpbar_imagesallocated <= 0 || ad->config.hpbar_imagestart < 0)
        {
                lm->LogA(L_WARN, "structures", arena, "hpbar: no images allocated");
                usehpbar = false;
        }

        for (a = 0; a < STRUCTURES_FREQS; a++)
        {
                snprintf(buf, sizeof(buf), "structures_freq%d", a);

                fdata = GetFreqData(arena, a);

                fdata->arena = arena;
                fdata->freq = a;

                fdata->lastUnderAttack = 0;
                fdata->repairRate = 1000;
                fdata->respawnRate = 1000;
                fdata->rewardBonus = 0;

                fdata->structureCount = 0;
                fdata->structureCountInitial = 0;

                fdata->structureTotalInitialHealth = 0;
                fdata->structureTotalHealth = 0;

                fdata->percentageDisplay_percentageLVZ = -1;

		fdata->config.freqName = util->cfgGetStrAlloc(ch, buf, "FreqName");
                fdata->config.underAttackMessage = util->cfgGetStrAlloc(ch, buf, "UnderAttackMessage");
                fdata->config.underAttackSound = cfg->GetInt(ch, buf, "UnderAttackSound", 0);
                fdata->config.underAttackReplayTicks = cfg->GetInt(ch, buf, "UnderAttackReplayTicks", 0);

                fdata->config.percentageDisplay_imageStart = cfg->GetInt(ch, buf, "PercentageDisplay_ImageStart", -1);
                fdata->config.percentageDisplay_imagePercentage = cfg->GetInt(ch, buf, "PercentageDisplay_ImagePercentage", -1);
                fdata->config.percentageDisplay_objectStart = cfg->GetInt(ch, buf, "PercentageDisplay_ObjectStart", -1);

                MIN(fdata->config.underAttackSound, 0);
                MIN(fdata->config.underAttackReplayTicks, 0);


                //read structures types data
                for (b = 0; b < STRUCTURES_STRUCTURETYPES; b++)
                {
                        snprintf(buf, sizeof(buf), "structures_freq%dtype_%s", a, structureTypes_settingName[b]);
                        struct structureTypeData *stdata = GetStructureTypeData(arena, a, b);

                        stdata->arena = arena;
                        stdata->freq = a;
                        stdata->type = b;

                        stdata->structureCount = 0;
                        stdata->structureCountLVZ = -1;
                        stdata->structureCountOld = -1;
                        stdata->structureCountInitial = 0;

                        stdata->structureTotalHealth = 0;
                        stdata->structureTotalInitialHealth = 0;

                        stdata->config.ZeroToOneObjectid = cfg->GetInt(ch, buf, "ZeroToOneObjectid", -1);
                        stdata->config.OneToZeroObjectid = cfg->GetInt(ch, buf, "OneToZeroObjectid", -1);

                        stdata->config.ZeroToOneEnemySound = cfg->GetInt(ch, buf, "ZeroToOneEnemySound", 0);
                        stdata->config.OneToZeroEnemySound = cfg->GetInt(ch, buf, "OneToZeroEnemySound", 0);
                        stdata->config.ZeroToOneTeamSound = cfg->GetInt(ch, buf, "ZeroToOneTeamSound", 0);
                        stdata->config.OneToZeroTeamSound = cfg->GetInt(ch, buf, "OneToZeroTeamSound", 0);

			stdata->config.ZeroToOneEnemyMessage = util->cfgGetStrAlloc(ch, buf,"ZeroToOneEnemyMessage");
                        stdata->config.OneToZeroEnemyMessage = util->cfgGetStrAlloc(ch, buf, "OneToZeroEnemyMessage");
                        stdata->config.ZeroToOneTeamMessage = util->cfgGetStrAlloc(ch, buf, "ZeroToOneTeamMessage");
                        stdata->config.OneToZeroTeamMessage = util->cfgGetStrAlloc(ch, buf, "OneToZeroTeamMessage");

                        MIN(stdata->config.ZeroToOneObjectid, -1);
                        MIN(stdata->config.OneToZeroObjectid, -1);

                        MIN(stdata->config.ZeroToOneEnemySound, 0);
                        MIN(stdata->config.OneToZeroEnemySound, 0);
                        MIN(stdata->config.ZeroToOneTeamSound, 0);
                        MIN(stdata->config.OneToZeroTeamSound, 0);
                }
        }

        for (a = 0; a < STRUCTURES_MAXIMUMSTRUCTURES; a++)
        {
                snprintf(buf, sizeof(buf), "Struct%d", a);
                tmpS = cfg->GetStr(ch, "structures", buf);
                fdata = NULL;

                if (!tmpS) break; // Reached end. (or a gap, which would be an user error in the config)

                sdata = amalloc(sizeof(struct structureData));
                sdata->typeID = HashAdd(ad->structures, tmpS, sdata);

                snprintf(buf, sizeof(buf), "structure_%s", sdata->typeID); //get the ini section, assuming config manager ignores case

                //Set up non configuration variables for the first time
                sdata->arena = arena;
                sdata->alive = false;
                sdata->health = 0;
                sdata->structureLVZState = STRUCTURESTATE_HIDDEN;
                sdata->hpbar_oldImageoffset = -1;
                sdata->hpbar_objectid = -1;
                sdata->lastDeath = 0;


                sdata->config.freq = cfg->GetInt(ch, buf, "Freq", 9999);
                tgtFreq.u.freq.freq = sdata->config.freq;

                sdata->config.x = cfg->GetInt(ch, buf, "X", -1) << 4;
                sdata->config.y = cfg->GetInt(ch, buf, "Y", -1) << 4;
                sdata->config.width = cfg->GetInt(ch, buf, "Width", 3) << 4; //optional
                sdata->config.height = cfg->GetInt(ch, buf, "Height", 3) << 4; //optional

                sdata->config.objectidLT100 = cfg->GetInt(ch, buf, "ObjectLessThen100", -1);
                sdata->config.objectidLT50 = cfg->GetInt(ch, buf, "ObjectLessThen50", -1); //optional
                sdata->config.objectidLT25 = cfg->GetInt(ch, buf, "ObjectLessThen25", -1); //optional

                sdata->config.destroyedObjectid = cfg->GetInt(ch, buf, "ObjectDestroyed", -1); //optional
                sdata->config.destroyedXOffset = cfg->GetInt(ch, buf, "DestroyedXOffset", 0); //optional
                sdata->config.destroyedYOffset = cfg->GetInt(ch, buf, "DestroyedXOffset", 0); //optional

                sdata->config.respawnSound = cfg->GetInt(ch, buf, "RespawnSound", 0); //optional; destroyed -> alive
                sdata->config.damaged50to49Sound = cfg->GetInt(ch, buf, "Damaged50Sound", 0); //optional;
                sdata->config.damaged25to24Sound = cfg->GetInt(ch, buf, "Damaged25Sound", 0); //optional;

		#define DOCFG(VAR, CFGVAL, DEF) \
		snprintf(buf2, sizeof(buf2), "structure::%s:" CFGVAL, sdata->typeID); \
		param.pointer = sdata; \
		param.pointer2 = &VAR; \
		attrman->Lock(); \
		attrman->RegisterCallback(buf2, SetValueCB, param); /* the setvalue below puts it into the config*/ \
		attrman->SetValue(&tgtFreq, settingSetter, buf2, true, ATTRMAN_NO_SHIP, cfg->GetInt(ch, buf, CFGVAL, DEF)); \
		attrman->UnLock();

		DOCFG(sdata->config.initialHealth, "Health", 100)


                sdata->config.explosionObjectid = cfg->GetInt(ch, buf, "ObjectExplosion", -1); //optional
                sdata->config.explosionSound = cfg->GetInt(ch, buf, "ExplosionSound", -1); //optional
                sdata->config.explosionXOffset = cfg->GetInt(ch, buf, "ExplosionXOffset", 0); //optional
                sdata->config.explosionYOffset = cfg->GetInt(ch, buf, "ExplosionYOffset", 0); //optional
                sdata->config.explosionDamageRange = cfg->GetInt(ch, buf, "ExplosionDamageRange", 0); //optional


                DOCFG(sdata->config.repairLT100, "RepairLessThen100", -1); //optional
                DOCFG(sdata->config.repairLT50, "RepairLessThen50", -1); //optional
                DOCFG(sdata->config.repairLT25, "RepairLessThen25", -1); //optional

		DOCFG(sdata->config.respawnTicks, "Respawn", 0); // < 0 = no respawn
		DOCFG(sdata->config.rewardBonus, "RewardBonus", 0);

                sdata->config.hpbar_enabled = !!cfg->GetInt(ch, buf, "ShowHealthBar", false);

		DOCFG(sdata->config.isBase, "IsBase", 0);
		DOCFG(sdata->config.gameDependent, "GameDependent", 0);
		DOCFG(sdata->config.teamDamage, "TeamDamage", 0);
		sdata->config.spawnOnStart = cfg->GetInt(ch, buf, "SpawnOnStart", true);

                sdata->config.respawnEnemyMessage = util->cfgGetStrAlloc(ch, buf, "RespawnEnemyMessage");
                sdata->config.explosionEnemyMessage = util->cfgGetStrAlloc(ch, buf,  "ExplosionEnemyMessage");
                sdata->config.damaged50to49EnemyMessage = util->cfgGetStrAlloc(ch, buf, "Damaged50EnemyMessage");
                sdata->config.damaged25to24EnemyMessage = util->cfgGetStrAlloc(ch, buf, "Damaged25EnemyMessage");

                sdata->config.respawnTeamMessage = util->cfgGetStrAlloc(ch, buf, "RespawnTeamMessage");
                sdata->config.explosionTeamMessage = util->cfgGetStrAlloc(ch, buf, "ExplosionTeamMessage");
                sdata->config.damaged50to49TeamMessage = util->cfgGetStrAlloc(ch, buf, "Damaged50TeamMessage");
                sdata->config.damaged25to24TeamMessage = util->cfgGetStrAlloc(ch, buf, "Damaged25TeamMessage");


                sdata->config.soundRange = cfg->GetInt(ch, buf, "SoundRange", 400);

                sdata->config.structureType = 0;


                if (sdata->config.initialHealth < 1)
                {
                        //health must be 1 or higher, else structures will respawn endless
                        lm->LogA(L_WARN, "structures", arena, "%s bad value for Health (%d)", sdata->typeID, sdata->config.initialHealth);
                        sdata->config.initialHealth = 100;
                }
                sdata->health = sdata->config.initialHealth;

                // Structure type
                for (b = 0; b < STRUCTURES_STRUCTURETYPES; b++)
                {
                        tmpInt = cfg->GetInt(ch, buf, structureTypes_settingNameIs[b], 0);

                        if (tmpInt)
                        {
                                sdata->config.structureType |= 1 << b;

                                if (sdata->config.freq >= 0 && sdata->config.freq < STRUCTURES_FREQS)
                                {
                                        ad->structureTypes[sdata->config.freq][b].structureCountInitial++;
                                        ad->structureTypes[sdata->config.freq][b].structureTotalInitialHealth += sdata->config.initialHealth;
                                }
                        }
                }

                tmpS = cfg->GetStr(ch, buf, "Region");
                if (tmpS)
                {
                        sdata->config.region = mapdata->FindRegionByName(arena, tmpS);

                        if (!sdata->config.region)
                                lm->LogA(L_WARN, "structures", arena, "%s region missing from map (%s)", sdata->typeID, tmpS);
                }
                else
                {
                        lm->LogA(L_WARN, "structures", arena, "%s bad value for region", sdata->typeID);
                }

                if (!usehpbar)
                        sdata->config.hpbar_enabled = false;

                if (sdata->config.hpbar_enabled)
                {
                        sdata->hpbar_objectid = ad->config.hpbar_objectstart + ad->hpbar_objectTicker;
                        ad->hpbar_objectTicker = (ad->hpbar_objectTicker + 1) % ad->config.hpbar_objectsallocated;
                }

                if (sdata->config.objectidLT100 < 0)
                {
                        lm->LogA(L_WARN, "structures", arena, "%s bad value for ObjectLessThen100 (%d)", sdata->typeID, sdata->config.objectidLT100);
                        sdata->config.objectidLT100 = -1;
                }

                if (sdata->config.objectidLT50 < 0)
                        sdata->config.objectidLT50 = sdata->config.objectidLT100;

                if (sdata->config.objectidLT25 < 0)
                        sdata->config.objectidLT25 = sdata->config.objectidLT50;

                MIN(sdata->config.respawnTicks, 0);

                MIN(sdata->config.destroyedObjectid, -1);
                MIN(sdata->config.explosionObjectid,1);

                MIN(sdata->config.respawnSound, 0);
                MIN(sdata->config.explosionSound, 0);
                MIN(sdata->config.damaged50to49Sound, 0);
                MIN(sdata->config.damaged25to24Sound ,0);
                MIN(sdata->config.explosionDamageRange, 0);

                MIN(sdata->config.soundRange, 0);

                if (sdata->config.repairLT100 < 0) sdata->config.repairLT100 = 0;
                if (sdata->config.repairLT50 < 0) sdata->config.repairLT50 = sdata->config.repairLT100;
                if (sdata->config.repairLT25 < 0) sdata->config.repairLT25 = sdata->config.repairLT50;

                //1024 * 16 - 1 = 16383
                if (sdata->config.x < 0 || sdata->config.x > 16383)
                {
                        lm->LogA(L_WARN, "structures", arena, "%s bad value for X (%d)", sdata->typeID, sdata->config.x >> 4);
                        sdata->config.x = 512;
                }
                if (sdata->config.y < 0 || sdata->config.y > 16383)
                {
                        lm->LogA(L_WARN, "structures", arena, "%s bad value for  Y (%d)", sdata->typeID, sdata->config.y >> 4);
                        sdata->config.y = 512;
                }

                //Freq
                if (sdata->config.freq < 0 || sdata->config.freq > 9999)
                {
                        lm->LogA(L_WARN, "structures", arena, "%s bad value for Freq (%d)", sdata->typeID, sdata->config.freq);
                        sdata->config.freq = 0;
                }

                if (sdata->config.width < 0)
                {
                        sdata->config.width = 0;
                }
                else if (sdata->config.x + sdata->config.width > 16368)
                {
                        lm->LogA(L_WARN, "structures", arena, "%s bad value for Width (%d)", sdata->typeID, sdata->config.width);
                        sdata->config.width = 0;
                }

                if (sdata->config.height < 0)
                {
                        sdata->config.height = 0;
                }
                else if (sdata->config.y + sdata->config.height > 16368)
                {
                        lm->LogA(L_WARN, "structures", arena, "%s bad value for Height (%d)", sdata->typeID, sdata->config.height);
                        sdata->config.height = 0;
                }

                if (sdata->config.x + sdata->config.explosionXOffset < 0 || sdata->config.x + sdata->config.explosionXOffset > 16368)
                {
                        lm->LogA(L_WARN, "structures", arena, "%s bad value for ExplosionXOffset (%d)", sdata->typeID, sdata->config.explosionXOffset);
                        sdata->config.explosionXOffset = 0;
                }

                if (sdata->config.y + sdata->config.explosionYOffset < 0 || sdata->config.y + sdata->config.explosionYOffset > 16368)
                {
                        lm->LogA(L_WARN, "structures", arena, "%s bad value for ExplosionYOffset (%d)", sdata->typeID, sdata->config.explosionYOffset);
                        sdata->config.explosionYOffset = 0;
                }

                if (sdata->config.x + sdata->config.destroyedXOffset < 0 || sdata->config.x + sdata->config.destroyedXOffset > 16368)
                {
                        lm->LogA(L_WARN, "structures", arena, "%s bad value for DestroyedXOffset (%d)", sdata->typeID, sdata->config.destroyedXOffset);
                        sdata->config.destroyedXOffset = 0;
                }

                if (sdata->config.y + sdata->config.destroyedYOffset < 0 || sdata->config.y + sdata->config.destroyedYOffset > 16368)
                {
                        lm->LogA(L_WARN, "structures", arena, "%s bad value for DestroyedYOffset (%d)", sdata->typeID, sdata->config.destroyedYOffset);
                        sdata->config.destroyedYOffset = 0;
                }

                if (sdata->config.freq >= STRUCTURES_FREQS) sdata->config.isBase = false;

                if (sdata->config.rewardBonus < 0)
                {
                        lm->LogA(L_WARN, "structures", arena, "%s bad value for RewardBonus (%d)", sdata->typeID, sdata->config.rewardBonus);
                        sdata->config.rewardBonus = 0;
                }

        }



        cmd->AddCommand("structuresoptimalization", Cstructuresoptimalization, arena, Cstructuresoptimalization_help);
}

static int InitializeArena_HashEnum_structures (const char *key, void *val, void *clos)
{
        return 0;
}

/** Initialize an arena for the first time */
static void InitializeArena (Arena *arena)
{
        assertlm(arena);

        cmd->RemoveCommand("structuresoptimalization", Cstructuresoptimalization, arena);

        struct arenaData *ad = GetArenaData(arena);
        assertlm(!ad->running && !ad->initialized); // An arena can only be initialized once

        CleanupArena(arena);
        ReadConfig(arena);
        //Structure data is ready after this point

        HashEnum(ad->structures, InitializeArena_HashEnum_structures, NULL);


        ad->initialized = true;


        ml->SetTimer(StructureRefreshTimer, STRUCTURES_STRUCTUREREFRESH_TIMER, STRUCTURES_STRUCTUREREFRESH_TIMER, arena, arena);
}

static int CleanupArena_HashEnum_structures (const char *key, void *val, void *clos)
{
	char buf[256];
        struct structureData *sdata = val;
        if (!sdata) return 1;

	attrman->Lock();
	#define UNREGCFG(CFGVAL) \
	snprintf(buf, sizeof(buf), "structure::%s:" CFGVAL, sdata->typeID); \
	attrman->UnregisterCallback(buf, SetValueCB);

	UNREGCFG("Health")
	UNREGCFG("RepairLessThen100")
	UNREGCFG("RepairLessThen50")
	UNREGCFG("RepairLessThen25")
	UNREGCFG("Respawn")
	UNREGCFG("RewardBonus")
	UNREGCFG( "IsBase")
	UNREGCFG("GameDependent")
	UNREGCFG("TeamDamage")
	attrman->UnLock();


        tFREE(sdata->config.respawnEnemyMessage);
        tFREE(sdata->config.explosionEnemyMessage);
        tFREE(sdata->config.damaged50to49EnemyMessage);
        tFREE(sdata->config.damaged25to24EnemyMessage);

        tFREE(sdata->config.respawnTeamMessage);
        tFREE(sdata->config.explosionTeamMessage);
        tFREE(sdata->config.damaged50to49TeamMessage);
        tFREE(sdata->config.damaged25to24TeamMessage);

        if (sdata->arena && sdata->config.region)
        {
                damage->RemoveRegion(sdata->arena, sdata->config.region);
        }

        return 1;
}

/** Clean up the arena so it can be destroyed */
static void CleanupArena(Arena *arena)
{
        assertlm(arena);
        struct arenaData *ad = GetArenaData(arena);
        assertlm(!ad->running);

        int a, b;

        ml->ClearTimer(StructureRefreshTimer, arena);

        DO_CBS(CB_STRUCTURES_DATADESTROY, arena, StructureDataDestroyFunc, (arena));

        ad->initialized = false;

        //struct structureData *sdata;
        struct freqData *fdata;
        struct structureTypeData *stdata;

        for (a = 0; a < STRUCTURES_FREQS; a++)
        {
                fdata = GetFreqData(arena, a);

                tFREE(fdata->config.freqName);
                tFREE(fdata->config.underAttackMessage);

                for (b = 0; b < STRUCTURES_STRUCTURETYPES; b++)
                {
                        stdata = GetStructureTypeData(arena, a, b);

                        tFREE(stdata->config.ZeroToOneEnemyMessage);
                        tFREE(stdata->config.ZeroToOneTeamMessage);
                        tFREE(stdata->config.OneToZeroEnemyMessage);
                        tFREE(stdata->config.OneToZeroTeamMessage);
                }
        }


        if (ad->structures)
        {
                HashEnum(ad->structures, CleanupArena_HashEnum_structures, NULL);
                HashFree(ad->structures);
                ad->structures = NULL;
        }


}
/*
static void ArenaAction(Arena *arena, int action)
{
        if (action == AA_CREATE && arena->status < ARENA_CLOSING)
        {
                assertlm(arena);
                InitializeArena(arena);
        }
        else if (action == AA_DESTROY)
        {
                assertlm(arena);
                struct arenaData *ad = GetArenaData(arena);
                if (ad->running)
                        StopGame(arena);

                CleanupArena(arena);
        }
}*/

static int StructureRefreshTimer_HashEnum_structures (const char *key, void *val, void *clos)
{
        struct structureData *sdata = val;
        Arena *arena = sdata->arena;
        assertlm(sdata->config.freq >= 0);

        struct arenaData *ad = GetArenaData(arena);

        unsigned int repairRate = 1000;
        unsigned int respawnRate = 1000;
        int repairPoints;

        //set variables
        if (sdata->config.freq < STRUCTURES_FREQS)
        {
                struct freqData *fdata = GetFreqData(arena, sdata->config.freq);

                repairRate = fdata->repairRate;
                respawnRate = fdata->respawnRate;

        }

        //Dont repair / respawn structure which live inside the game while the game is not running.
        if (!(sdata->config.gameDependent && !ad->running))
        {
                if (sdata->alive)
                {

                        //do not auto repair if repairRate = 0;
                        if (repairRate)
                        {
                                repairPoints = 0;

                                if (sdata->health >= sdata->config.initialHealth >> 1) //after 50%
                                {
                                        repairPoints = ceil((sdata->config.repairLT100 * repairRate) / 1000.0);
                                }
                                else if (sdata->health >= sdata->config.initialHealth >> 2) //after 25%; not after 50%
                                {
                                        repairPoints = ceil((sdata->config.repairLT50 * repairRate) / 1000.0);
                                }
                                else //after 0%; not after 25%
                                {
                                        repairPoints = ceil((sdata->config.repairLT25 * repairRate) / 1000.0);
                                }

                                MIN(repairPoints, 0);

                                SetHealth(sdata, sdata->health + repairPoints, false);
                        }
                }
                else //not alive
                {
                        if (!sdata->spawnCount && sdata->config.spawnOnStart) //structure has not spawned yet
                        {
                                SpawnStructure(sdata, true);
                        }
                        else
			{
				if (sdata->config.respawnTicks > 0)
				{
					if (sdata->respawnTicksLeft <= 0)  //structure is ready to be respawned
		                        {
		                                SpawnStructure(sdata, false);
		                        }
		                        else if (respawnRate) //respawnRate = 0 means no respawn
		                        {
		                                sdata->respawnTicksLeft -= STRUCTURES_STRUCTUREREFRESH_TIMER * (respawnRate / 1000.0); // int * (int / double) = int * double = double
		                        }
	                        }
                        }
                }
        }
        return 0;
}


/** Called every X seconds; Does auto-repair, auto-respawn, checks for win */
static int StructureRefreshTimer(void *arena_)
{


        Arena *arena = arena_;
        assertlm(arena);

        struct arenaData *ad = GetArenaData(arena);
        assertlm(ad->initialized);

       // struct structureData *sdata;
        //unsigned int repairRate;
        //unsigned int respawnRate;
        //int repairPoints;

        int a;


        HashEnum(ad->structures, StructureRefreshTimer_HashEnum_structures, NULL);

        UpdatePercentageFx(arena);

        if (ad->running)
        {
                int freqsWithAliveStructuresCount = 0;
                int freq = -1;
                Target tgt;

                //check if only 1 playing freq has structures with isBase left
                for (a = 0; a < STRUCTURES_PLAYINGFREQS; a++)
                {
                	if (!ad->freqs[a].structureCountInitial) //special exception, the freq's structures have not spawned yet
                	{
                		freqsWithAliveStructuresCount = 2;
                		break;
                	}

                        if (ad->freqs[a].structureCount) //freq has no structures left
                        {
                                freqsWithAliveStructuresCount++;
                                freq = a;
                        }
                }

                if (freqsWithAliveStructuresCount == 0) //Tie
                {
                        StopGame(arena);
                        chat->SendArenaMessage(arena, "NOTICE: Game over. Tie game.");

                        if (ad->gameOverFunc)
                                ad->gameOverFunc(arena, NULL);
                }
                else if (freqsWithAliveStructuresCount == 1) //Winner
                {
                        assertlm(freq >= 0);

                        StopGame(arena);
                        RewardPoints(arena, freq); //contains arena message

                        if (ad->gameOverFunc)
                        {
                                tgt.type = T_FREQ;
                                tgt.u.freq.arena = arena;
                                tgt.u.freq.freq = freq;

                                ad->gameOverFunc(arena, &tgt);
                        }
                }
        }

        return 1;
}

static int StartGame_HashEnum_structures(const char *key, void *val, void *clos)
{
        struct structureData *sdata = val;
        if (sdata->config.gameDependent)
        {
                //reset the structure for a new game
                if (sdata->alive)
                {
                	if (sdata->config.spawnOnStart)
                	{
	                        //structure was not destroyed the last game, reset some things
	                        sdata->respawnTicksLeft = (double) sdata->config.respawnTicks;
	                        SetHealth(sdata, sdata->config.initialHealth, true);
                        }
                        else
                        {
                        	DestroyStructure(sdata, true); //silent
                        }
                }
                else
                {
                	if (sdata->config.spawnOnStart)
                        	SpawnStructure(sdata, true); //silent
                        else
                        {
                        	sdata->respawnTicksLeft = (double) sdata->config.respawnTicks;
                        }
                }
        }
        UpdateStructureFx(val, true);
        return 0;
}

static void StartGame (Arena *arena)
{

        assertlm(arena);
        struct arenaData *ad = GetArenaData(arena);
        assertlm(!ad->running);
        assertlm(ad->gameOverFunc);

        int a;


        for (a = 0; a < STRUCTURES_FREQS; a++)
        {
                struct freqData *fdata = GetFreqData(arena, a);

                fdata->lastUnderAttack = 0;
                fdata->repairRate = 1000;
                fdata->respawnRate = 1000;
                fdata->rewardBonus = 0;
        }


        Istaticturret *turrets = mm->GetInterface(I_STATICTURRET, arena);
        Igamecredits *credits = mm->GetInterface(I_GAMECREDITS, arena);
        Iadvbuy *advbuy = mm->GetInterface(I_ADVBUY, arena);
        Iharvest *harvest = mm->GetInterface(I_HARVEST, arena);

        if (turrets)
                turrets->StartGame(arena);

        if (credits)
                credits->StartGame(arena);

        if (advbuy)
                advbuy->StartGame(arena);

        if (harvest)
        	harvest->StartGame(arena);

        mm->ReleaseInterface(turrets);
        mm->ReleaseInterface(advbuy);
        mm->ReleaseInterface(credits);
        mm->ReleaseInterface(harvest);

//todo spawn area
        ad->running = true;

        HashEnum(ad->structures, StartGame_HashEnum_structures, NULL);

        ad->gameStartTime = current_ticks();


        DO_CBS(CB_STRUCTURES_GAMESTART, arena, StructuresGameStartFunc, (arena));

        if (ad->config.gameRunningObjectid >= 0)
                util->ObjToggle(arena, ad->config.gameRunningObjectid, 1, true);
}
static void StopGame(Arena *arena)
{
        assertlm(arena);
        struct arenaData *ad = GetArenaData(arena);
        assertlm(ad->running);
        struct structureTypeData *stdata;
        struct freqData *fdata;

        int a, b;

        //Useful shorthands, do not put a ; after these
        //#define tTOGGLESETON(a) { id[oU] = a; ons[oU] = 1; oU++; }
#define tTOGGLESETOFF(a) { id[oU] = a; ons[oU] = 0; oU++; }

        //the number of times the above two are used
        // 1 object per freq and type = stdata->config.ZeroToOneObjectid or stdata->config.OneToZeroObjectid
        // +1 = ad->config.gameRunningObjectid
        // 4 per freq for fdata->config.percentageDisplay_objectStart
#define tTOGGLESETUSED (STRUCTURES_FREQS * STRUCTURES_STRUCTURETYPES + 1 + 4 * STRUCTURES_FREQS)
        unsigned int oU = 0;
        short id[tTOGGLESETUSED]; //
        char ons[tTOGGLESETUSED]; //boolean


        ad->running = false;

        ad->gameStopTime = current_ticks();


        Istaticturret *turrets = mm->GetInterface(I_STATICTURRET, arena);
        Igamecredits *credits = mm->GetInterface(I_GAMECREDITS, arena);
        Iadvbuy *advbuy = mm->GetInterface(I_ADVBUY, arena);
        Iharvest *harvest = mm->GetInterface(I_HARVEST, arena);

        if (turrets)
                turrets->StopGame(arena);

        if (credits)
                credits->StopGame(arena);

        if (advbuy)
                advbuy->StopGame(arena);

        if (harvest)
                harvest->StopGame(arena);

        mm->ReleaseInterface(turrets);
        mm->ReleaseInterface(advbuy);
        mm->ReleaseInterface(credits);
        mm->ReleaseInterface(harvest);


        DO_CBS(CB_STRUCTURES_GAMESTOP, arena, StructuresGameStopFunc, (arena));

        if (ad->config.hideScreenObjectsOnGameStop)
        {

                for (a = 0; a < STRUCTURES_FREQS; a++)
                {
                        fdata = GetFreqData(arena, a);
                        if (fdata->config.percentageDisplay_objectStart >= 0)
                        {
                                tTOGGLESETOFF(fdata->config.percentageDisplay_objectStart)
                                tTOGGLESETOFF(fdata->config.percentageDisplay_objectStart + 1)
                                tTOGGLESETOFF(fdata->config.percentageDisplay_objectStart + 2)
                                tTOGGLESETOFF(fdata->config.percentageDisplay_objectStart + 3)
                        }
                        fdata->percentageDisplay_percentageLVZ = -1;

                        for (b = 0; b < STRUCTURES_STRUCTURETYPES; b++)
                        {
                                stdata = GetStructureTypeData(arena, a, b);

                                if (stdata->structureCountLVZ > 0)
                                {
                                        if (stdata->config.ZeroToOneObjectid >= 0)
                                                tTOGGLESETOFF(stdata->config.ZeroToOneObjectid)
                                        }
                                else // stdata->structureCountLVZ == 0
                                {
                                        if (stdata->config.OneToZeroObjectid >= 0)
                                                tTOGGLESETOFF(stdata->config.OneToZeroObjectid)
                                        }

                                stdata->structureCountLVZ = -1;
                        }
                }


                if (ad->config.gameRunningObjectid >= 0)
                        tTOGGLESETOFF(ad->config.gameRunningObjectid)

                        if (oU)
                                util->ObjToggleSet(arena, id, ons, oU + 1, true);
        }
        //#undef tTOGGLESETON
#undef tTOGGLESETOFF
#undef tTOGGLESETUSED

}
/** wrapper for when this is called by swappable game */
static void SwapGameStopGame(Arena *arena)
{

        assertlm(arena);
        struct arenaData *ad = GetArenaData(arena);

        if (ad->running)
                StopGame(arena);
}
/** wrapper for when this is called by swappable game */
static void SwapGameStartGame(Arena *arena, GameoverFunc gameOverFunc)
{

        assertlm(arena);
        struct arenaData *ad = GetArenaData(arena);


        ad->gameOverFunc = gameOverFunc;


        if (!ad->running)
                StartGame(arena);

}
static void RewardPoints(Arena *arena, int freq)
{

        assertlm(arena);
        struct arenaData *ad = GetArenaData(arena);
        Link *link;
        Player *p;

        assertlm(freq < STRUCTURES_FREQS);

        struct freqData *fdata = GetFreqData(arena, freq);

        int playing, onFreq;
        int intPoints;
        double points;
        int bonusPercent = 0;

        char teamMessage[256];
        char enemyMessage[256];

        if (stats)
        {
                pd->Lock();
                FOR_EACH_PLAYER(p)
                {
                        if (p->status == S_PLAYING && p->arena == arena && p->p_ship != SHIP_SPEC && IS_HUMAN(p))
                        {
                                playing++;
                                if (p->p_freq == freq)
                                        onFreq++;
                        }
                }
                pd->Unlock();

                //reward bonus
                points = ad->config.extraReward;

                //main reward
                if (ad->config.reward)
                        points += (playing * playing) * (ad->config.reward / 1000.0); // (int * int) * (int / double) = int * double = double

                //jackpot
                if (ad->config.useJackpot)
                {
                        jackpot = mm->GetInterface(I_JACKPOT, arena);
                        if (jackpot)
                        {
                                points += jackpot->GetJP(arena);
                                jackpot->ResetJP(arena);
                                mm->ReleaseInterface(jackpot);
                        }
                }

                //bonus
                if (freq < STRUCTURES_FREQS && fdata->rewardBonus)
                {
                        points += points * (fdata->rewardBonus / 1000.0);
                        bonusPercent = fdata->rewardBonus / 10;
                }

                //split points
                if (ad->config.splitPoints && onFreq > 0)
                {
                        points /= onFreq;
                }

                intPoints = (int) points;
        }
        else
        {
                lm->LogA(L_WARN, "structures", arena, "No points given for winning structures game, stats module is not loaded");
        }


        //prepare the messages
        if (freq < STRUCTURES_FREQS && fdata->config.freqName)
        {
                if (!stats)
                {
                        snprintf(teamMessage, sizeof(teamMessage), "%s wins. All structures destroyed.", fdata->config.freqName);
                        snprintf(enemyMessage, sizeof(enemyMessage), "%s wins. All structures destroyed.", fdata->config.freqName);
                }
                else
                {
                        if (bonusPercent)
                        {
                                snprintf(teamMessage, sizeof(teamMessage), "%s wins. All structures destroyed. Reward: %d (Bonus: %d%%)", fdata->config.freqName, intPoints, bonusPercent);
                                snprintf(enemyMessage, sizeof(enemyMessage), "%s wins. All structures destroyed. (%d points given, %d%% bonus))", fdata->config.freqName, intPoints, bonusPercent);
                        }
                        else
                        {
                                snprintf(teamMessage, sizeof(teamMessage), "%s wins. All structures destroyed. Reward: %d", fdata->config.freqName, intPoints);
                                snprintf(enemyMessage, sizeof(enemyMessage), "%s wins. All structures destroyed. (%d points given))", fdata->config.freqName, intPoints);
                        }
                }
        }
        else
        {
                if (!stats)
                {
                        snprintf(teamMessage, sizeof(teamMessage), "Freq %d wins. All structures destroyed.", freq);
                        snprintf(enemyMessage, sizeof(enemyMessage), "Freq %d wins. All structures destroyed.", freq);
                }
                else
                {
                        if (bonusPercent)
                        {
                                snprintf(teamMessage, sizeof(teamMessage), "Freq %d wins. All structures destroyed. Reward: %d (Bonus: %d%%)", freq, intPoints, bonusPercent);
                                snprintf(enemyMessage, sizeof(enemyMessage), "Freq %d wins. All structures destroyed. (%d points given, %d%% bonus))", freq, intPoints, bonusPercent);
                        }
                        else
                        {
                                snprintf(teamMessage, sizeof(teamMessage), "Freq %d wins. All structures destroyed. Reward: %d", freq, intPoints);
                                snprintf(enemyMessage, sizeof(enemyMessage), "Freq %d wins. All structures destroyed. (%d points given))", freq, intPoints);
                        }
                }
        }


        //send the messages, raise stats
        pd->Lock();
        FOR_EACH_PLAYER(p)
        {
                if (p->status == S_PLAYING && p->arena == arena)
                {
                        //
                        if (p->p_freq == freq)
                        {
                                chat->SendMessage(p, "%s", teamMessage);
                                if (p->p_ship != SHIP_SPEC && stats)
                                        stats->IncrementStat(p, STAT_FLAG_POINTS, intPoints);
                        }
                        else
                        {
                                chat->SendMessage(p, "%s", enemyMessage);
                        }
                }
        }
        pd->Unlock();

        if (stats)
                stats->SendUpdates(NULL);
}
/** Destroy a structure, do not use this if the structure is already destroyed */
static void DestroyStructure(struct structureData *sdata, bool silent)
{
        assertlm(sdata);
        assertlm(sdata->alive);
        Arena *arena = sdata->arena;
        struct arenaData *ad = GetArenaData(arena);
        assertlm(ad->initialized);

        int a;
        int oldhealth = sdata->health;


        sdata->alive = false;
        sdata->destroyCount++;
        sdata->health = 0;
        sdata->lastDeath = current_ticks();
        sdata->respawnTicksLeft = (double) sdata->config.respawnTicks;

        if (sdata->config.structureType)
        {
                for (a = 0; a < STRUCTURES_STRUCTURETYPES; a++)
                {
                        if (sdata->config.structureType & 1 << a)
                        {
                                struct structureTypeData *stdata = GetStructureTypeData(arena, sdata->config.freq, a);
                                stdata->structureCount--;

                                stdata->structureTotalHealth += sdata->health - oldhealth;
                        }
                }
        }

        if (sdata->config.freq < STRUCTURES_FREQS)
        {
                struct freqData *fdata = GetFreqData(arena, sdata->config.freq);

                if (sdata->config.isBase)
                {
                        fdata->structureCount--;
                        fdata->structureTotalHealth += sdata->health - oldhealth;
                }

                fdata->rewardBonus -= sdata->config.rewardBonus;
        }

        if (sdata->config.region)
                damage->RemoveRegion(arena, sdata->config.region);

        // hurt nearby players
        if (sdata->config.explosionDamageRange && !silent)
        {
                util->PrizeNearbyPlayers(arena, -PRIZE_FULLCHARGE, sdata->config.x, sdata->config.y, sdata->config.width, sdata->config.height, sdata->config.explosionDamageRange);
        }

        //update graphics, sound, etc
        UpdateStructureFx(sdata, false);

        DO_CBS(CB_STRUCTURES_STRUCTUREDESTROYED, arena, StructuresStructureDestroyedFunc, (sdata));
        UpdateStructureTypes(sdata);
}
static void DestroyStructureInterface(const struct structureData *sdata, bool silent)
{
        assertlm(sdata);
        DestroyStructure((struct structureData *) sdata, silent);
}
/** Spawn a structure, do not use this if the structure is alive */
static void SpawnStructure(struct structureData *sdata, bool silent)
{
        assertlm(sdata);
        assertlm(!sdata->alive);
        Arena *arena = sdata->arena;
        struct arenaData *ad = GetArenaData(arena);
        assertlm(ad->initialized);
        struct freqData *fdata = GetFreqData(arena, sdata->config.freq);

        int a;

        //reset health
        sdata->health = sdata->config.initialHealth;
        sdata->alive = true;
        sdata->spawnCount++;
        sdata->respawnTicksLeft = (double) sdata->config.respawnTicks;

        if (sdata->config.structureType)
        {
                for (a = 0; a < STRUCTURES_STRUCTURETYPES; a++)
                {
                        if (sdata->config.structureType & 1 << a)
                        {
                                struct structureTypeData *stdata = GetStructureTypeData(arena, sdata->config.freq, a);
                                stdata->structureCount++;

                                stdata->structureTotalHealth += sdata->config.initialHealth;
                        }
                }
        }

	if (sdata->spawnCount == 1) //first spawn
	{
		if (sdata->config.freq < STRUCTURES_FREQS && sdata->config.isBase) //add me to the freq's structure list
                {
                        fdata = GetFreqData(arena, sdata->config.freq);

                        fdata->structureCountInitial++;
                        fdata->structureTotalInitialHealth += sdata->config.initialHealth;
                }
	}

        if (sdata->config.freq < STRUCTURES_FREQS)
        {
                struct freqData *fdata = GetFreqData(arena, sdata->config.freq);

                if (sdata->config.isBase)
                {
                        fdata->structureCount++;
                        fdata->structureTotalHealth += sdata->config.initialHealth;
                }

                fdata->rewardBonus += sdata->config.rewardBonus;
        }

        if (sdata->config.region)
                damage->AddRegion(arena, sdata->config.region, StructureTileDamage, sdata);

        UpdateStructureFx(sdata, silent);

        DO_CBS(CB_STRUCTURES_STRUCTURESPAWNED, arena, StructuresStructureSpawnedFunc, (sdata));
        UpdateStructureTypes(sdata);
}
static void SpawnStructureInterface(const struct structureData *sdata, bool silent)
{
        assertlm(sdata);
        SpawnStructure((struct structureData *) sdata, silent);
}
/** Set the health for a structure; Use this function, do not set -> health directly */
static int SetHealth(struct structureData *sdata, int health, bool silent)
{


        assertlm(sdata);
        assertlm(sdata->alive);

        Arena *arena = sdata->arena;

        int a;
        int oldHealth, diffHealth;
        struct structureTypeData *stdata;



        MIN(health, 0);

        oldHealth = sdata->health;
        sdata->health = health;

        MIN(sdata->health, 0);
        MAX(sdata->health, sdata->config.initialHealth);

        diffHealth = sdata->health - oldHealth; //the final difference in health, after limits have been applied

        if (diffHealth)
        {
                if (sdata->config.structureType)
                {
                        for (a = 0; a < STRUCTURES_STRUCTURETYPES; a++)
                        {
                                if (sdata->config.structureType & 1 << a)
                                {
                                        stdata = GetStructureTypeData(arena, sdata->config.freq, a);

                                        stdata->structureTotalHealth += diffHealth;
                                }
                        }
                }
                if (sdata->config.freq < STRUCTURES_FREQS && sdata->config.isBase)
                {
                        struct freqData *fdata = GetFreqData(arena, sdata->config.freq);

                        fdata->structureTotalHealth += diffHealth;
                }
        }
        if (!sdata->health)
        {
                assertlm(sdata->alive);
                DestroyStructure(sdata, silent);
        }
        else
        {
                UpdateStructureFx(sdata, silent);
        }



        return diffHealth;
}
static int SetHealthInterface(const struct structureData *sdata, int health, bool silent)
{
        assertlm(sdata);
        return SetHealth((struct structureData *) sdata, health, silent);
}
/** Updates behavior for structure types that has nothing to do with GFX / SFX (Example: radar blacks out if you loose all your radar structures);
At the moment, this function only contains a callback */
static void UpdateStructureTypes(struct structureData *sdata)
{


        assertlm(sdata);
        Arena *arena = sdata->arena;

        int a;

        if (sdata->config.structureType)
        {
                for (a = 0; a < STRUCTURES_STRUCTURETYPES; a++)
                {
                        if (sdata->config.structureType & 1 << a)
                        {
                                struct structureTypeData *stdata = GetStructureTypeData(arena, sdata->config.freq, a);
                                assertlm(stdata->structureCount >= 0);

                                if (stdata->structureCountOld != stdata->structureCount)
                                {
                                        DO_CBS(CB_STRUCTURES_TYPEDATASTRUCTURECOUNTCHANGED, arena, StructuresTypeDataStructureCountChangedFunc, (stdata, stdata->structureCountOld));

                                        stdata->structureCountOld = stdata->structureCount;

                                }
                        }
                }
        }
}
/** This function reads all the data of a structure and updates the LVZ, sounds, messages, etc; This function should be called everytime a change is made */
static void UpdateStructureFx(struct structureData *sdata, bool silent)
{


        assertlm(sdata);
        Arena *arena = sdata->arena;
        struct arenaData *ad = GetArenaData(arena);

        int a;


        //Useful shorthands, do not put a ; after these
#define tTOGGLESETON(a) { id[oU] = a; ons[oU] = 1; oU++; }
#define tTOGGLESETOFF(a) { id[oU] = a; ons[oU] = 0; oU++; }

        //the number of times the above two are used
#define tTOGGLESETUSED 9
        unsigned int oU = 0;
        short id[tTOGGLESETUSED]; //
        char ons[tTOGGLESETUSED]; //boolean

        LinkedList freqSet = LL_INITIALIZER; //on the same freq as the structure
        LinkedList enemySet = LL_INITIALIZER; // not on the same freq

        util->GetFreqSet(&freqSet, arena, sdata->config.freq);
        util->GetNotonFreqSet(&enemySet, arena, sdata->config.freq);

        STRUCTURESTATE newStructureLVZState = STRUCTURESTATE_HIDDEN; //if all fails, use hidden
        STRUCTURESTATE oldStructureLVZState = sdata->structureLVZState;

        int oldStructureObjectid = -1;
        int newStructureObjectid = -1;

        int imageOffset;

        //find out the new structure state
        if (sdata->alive)
        {
                if (sdata->health < sdata->config.initialHealth >> 2)
                {
                        //under 25%
                        newStructureLVZState = STRUCTURESTATE_FOURTHHEALTH;
                        newStructureObjectid = sdata->config.objectidLT25;

                }
                else if (sdata->health < sdata->config.initialHealth >> 1)
                {
                        //under 50%
                        newStructureLVZState = STRUCTURESTATE_HALFHEALTH;
                        newStructureObjectid = sdata->config.objectidLT50;

                }
                else
                {
                        //over 50%
                        newStructureLVZState = STRUCTURESTATE_FULLHEALTH;
                        newStructureObjectid = sdata->config.objectidLT100;


                }
        }
        else //not alive
        {
                newStructureLVZState = STRUCTURESTATE_DESTROYED;
                newStructureObjectid = sdata->config.destroyedObjectid;
        }


        sdata->structureLVZState = newStructureLVZState;


        //do nothing if the state is unchanged
        if (oldStructureLVZState != newStructureLVZState)
        {
                // hide the old state
                if (oldStructureLVZState == STRUCTURESTATE_FULLHEALTH)
                {
                        oldStructureObjectid = sdata->config.objectidLT100;
                }
                else if (oldStructureLVZState == STRUCTURESTATE_HALFHEALTH)
                {
                        oldStructureObjectid = sdata->config.objectidLT50;

                }
                else if (oldStructureLVZState == STRUCTURESTATE_FOURTHHEALTH)
                {
                        oldStructureObjectid = sdata->config.objectidLT25;

                }
                else if (oldStructureLVZState == STRUCTURESTATE_DESTROYED)
                {
                        oldStructureObjectid = sdata->config.destroyedObjectid;
                }

                //show the new state
                if (newStructureLVZState == STRUCTURESTATE_FULLHEALTH)
                {


                }
                else if (newStructureLVZState == STRUCTURESTATE_HALFHEALTH)
                {
                        if (oldStructureLVZState == STRUCTURESTATE_FULLHEALTH) // FULLHEALTH -> HALFHEALTH
                        {
                                if (!silent)
                                {
                                        if (sdata->config.damaged50to49Sound)
                                                util->SFXToNearbyPlayers (arena, sdata->config.damaged50to49Sound, sdata->config.x, sdata->config.y, sdata->config.width, sdata->config.height, sdata->config.soundRange);

                                        if (ad->running)
                                        {
                                                if (sdata->config.damaged50to49TeamMessage)
                                                        chat->SendSetMessage(&freqSet, "%s", sdata->config.damaged50to49TeamMessage);

                                                if (sdata->config.damaged50to49EnemyMessage)
                                                        chat->SendSetMessage(&enemySet, "%s", sdata->config.damaged50to49EnemyMessage);
                                        }
                                }
                        }

                }
                else if (newStructureLVZState == STRUCTURESTATE_FOURTHHEALTH)
                {
                        if (oldStructureLVZState == STRUCTURESTATE_HALFHEALTH || oldStructureLVZState == STRUCTURESTATE_FULLHEALTH) // HALFHEALTH -> FOURTHHEALTH
                        {
                                if (!silent)
                                {
                                        if (sdata->config.damaged25to24Sound)
                                                util->SFXToNearbyPlayers (arena, sdata->config.damaged25to24Sound, sdata->config.x, sdata->config.y, sdata->config.width, sdata->config.height, sdata->config.soundRange);

                                        if (ad->running)
                                        {
                                                if (sdata->config.damaged25to24TeamMessage)
                                                        chat->SendSetMessage(&freqSet, "%s", sdata->config.damaged25to24TeamMessage);

                                                if (sdata->config.damaged25to24EnemyMessage)
                                                        chat->SendSetMessage(&enemySet, "%s", sdata->config.damaged25to24EnemyMessage);
                                        }
                                }
                        }

                }
                else if (newStructureLVZState == STRUCTURESTATE_DESTROYED)
                {

                        if (oldStructureLVZState > STRUCTURESTATE_DESTROYED)
                        {
                                //only show explosion if the old lvz state was not hidden

                                if (sdata->config.explosionObjectid >= 0)
                                {
                                        util->ObjMove(arena, sdata->config.explosionObjectid, sdata->config.x + sdata->config.explosionXOffset, sdata->config.y + sdata->config.explosionYOffset, 0, 0, false);

                                        tTOGGLESETON(sdata->config.explosionObjectid)
                                }

                                if (sdata->config.hpbar_enabled && sdata->hpbar_visible)
                                {
                                        tTOGGLESETOFF(sdata->hpbar_objectid);

                                        sdata->hpbar_visible = false;
                                        sdata->hpbar_oldImageoffset = -1;

                                }

                                if (!silent)
                                {
                                        if (sdata->config.explosionSound)
                                                util->SFXToNearbyPlayers (arena, sdata->config.explosionSound, sdata->config.x, sdata->config.y, sdata->config.width, sdata->config.height, sdata->config.soundRange);

                                        if (ad->running)
                                        {
                                                if (sdata->config.explosionTeamMessage)
                                                        chat->SendSetMessage(&freqSet, "%s", sdata->config.explosionTeamMessage);

                                                if (sdata->config.explosionEnemyMessage)
                                                        chat->SendSetMessage(&enemySet, "%s", sdata->config.explosionEnemyMessage);
                                        }
                                }
                        }
                }

                //Same as above, but then for all alive states
                if (newStructureLVZState > STRUCTURESTATE_DESTROYED)
                {
                        if (oldStructureLVZState <= STRUCTURESTATE_DESTROYED)
                        {
                                //destroyed -> alive
                                if (!silent)
                                {
                                        if (sdata->config.respawnSound)
                                                util->SFXToNearbyPlayers (arena, sdata->config.respawnSound, sdata->config.x, sdata->config.y, sdata->config.width, sdata->config.height, sdata->config.soundRange);

                                        if (ad->running)
                                        {
                                                if (sdata->config.respawnTeamMessage)
                                                        chat->SendSetMessage(&freqSet, "%s", sdata->config.respawnTeamMessage);

                                                if (sdata->config.respawnEnemyMessage)
                                                        chat->SendSetMessage(&enemySet, "%s", sdata->config.respawnEnemyMessage);
                                        }
                                }


                                if (sdata->config.hpbar_enabled && !sdata->hpbar_visible)
                                {
                                        util->ObjMove(arena, sdata->hpbar_objectid, sdata->config.x + ad->config.hpbar_xoffset, sdata->config.y + ad->config.hpbar_yoffset, 0, 0, true);
                                        tTOGGLESETON(sdata->hpbar_objectid);

                                        sdata->hpbar_visible = true;

                                }
                        }
                }


                //do not turn the same object off-on
                if (oldStructureObjectid != newStructureObjectid)
                {

                        if (newStructureObjectid >= 0)
                        {

                                if (newStructureLVZState == STRUCTURESTATE_DESTROYED)
                                {
                                        util->ObjMove(arena, sdata->config.destroyedObjectid, sdata->config.x + sdata->config.destroyedXOffset, sdata->config.y + sdata->config.destroyedYOffset, 0, 0, true);
                                }
                                else
                                {
                                        util->ObjMove(arena, newStructureObjectid, sdata->config.x, sdata->config.y, 0, 0, true);
                                }

                                tTOGGLESETON(newStructureObjectid);
                        }

                        if (oldStructureObjectid >= 0)
                                tTOGGLESETOFF(oldStructureObjectid)
                        }
        }

        // Update the health bar
        if (sdata->hpbar_visible)
        {
                assertlm(sdata->config.hpbar_enabled);

                imageOffset = sdata->health * (ad->config.hpbar_imagesallocated - 1) / sdata->config.initialHealth;

                if (imageOffset != sdata->hpbar_oldImageoffset)
                {
                        util->ObjImage(arena, sdata->hpbar_objectid, ad->config.hpbar_imagestart + imageOffset, true);

                        sdata->hpbar_oldImageoffset = imageOffset;

                }
        }

        // Update the structure types this building is in
        if (ad->running && sdata->config.structureType)
        {
                for (a = 0; a < STRUCTURES_STRUCTURETYPES; a++)
                {
                        if (sdata->config.structureType & 1 << a)
                        {
                                struct structureTypeData *stdata = GetStructureTypeData(arena, sdata->config.freq, a);
                                assertlm(stdata->structureCount >= 0);

                                //structureCount changed
                                if (stdata->structureCount != stdata->structureCountLVZ)
                                {
                                        if (stdata->structureCountLVZ && !stdata->structureCount) //1+ to 0  or  -1 to 0
                                        {
                                                if (stdata->config.ZeroToOneObjectid >= 0)
                                                        tTOGGLESETOFF(stdata->config.ZeroToOneObjectid)

                                                        if (stdata->config.OneToZeroObjectid >= 0)
                                                                tTOGGLESETON(stdata->config.OneToZeroObjectid)


                                                                if (!silent)
                                                                {
                                                                        if (stdata->config.OneToZeroTeamSound)
                                                                                chat->SendSetSoundMessage(&freqSet, stdata->config.OneToZeroTeamSound, "");

                                                                        if (stdata->config.OneToZeroEnemySound)
                                                                                chat->SendSetSoundMessage(&enemySet, stdata->config.OneToZeroEnemySound, "");


                                                                        if (stdata->config.OneToZeroTeamMessage)
                                                                                chat->SendSetMessage(&freqSet, "%s", stdata->config.OneToZeroTeamMessage);

                                                                        if (stdata->config.OneToZeroEnemyMessage)
                                                                                chat->SendSetMessage(&enemySet, "%s", stdata->config.OneToZeroEnemyMessage);
                                                                }
                                        }
                                        else if (stdata->structureCountLVZ <= 0 && stdata->structureCount) // 0 to 1+    or   -1 to 1+
                                        {
                                                if (stdata->config.OneToZeroObjectid >= 0)
                                                        tTOGGLESETOFF(stdata->config.OneToZeroObjectid)

                                                        if (stdata->config.ZeroToOneObjectid >= 0)
                                                                tTOGGLESETON(stdata->config.ZeroToOneObjectid)

                                                                if (!silent)
                                                                {
                                                                        if (stdata->config.ZeroToOneTeamSound)
                                                                                chat->SendSetSoundMessage(&freqSet, stdata->config.ZeroToOneTeamSound, "");

                                                                        if (stdata->config.ZeroToOneEnemySound)
                                                                                chat->SendSetSoundMessage(&enemySet, stdata->config.ZeroToOneEnemySound, "");


                                                                        if (stdata->config.ZeroToOneTeamMessage)
                                                                                chat->SendSetMessage(&freqSet, "%s", stdata->config.ZeroToOneTeamMessage);

                                                                        if (stdata->config.ZeroToOneEnemyMessage)
                                                                                chat->SendSetMessage(&enemySet, "%s", stdata->config.ZeroToOneEnemyMessage);
                                                                }
                                        }

                                        stdata->structureCountLVZ = stdata->structureCount;

                                }
                        }
                }
        }

        if (oU)
                util->ObjToggleSet(arena, id, ons, oU + 1, true);

        LLEmpty(&freqSet);
        LLEmpty(&enemySet);

#undef tTOGGLESETON
#undef tTOGGLESETOFF
#undef tTOGGLESETUSED
}
static void UpdatePercentageFx(Arena *arena)
{

        assertlm(arena);
        struct arenaData *ad = GetArenaData(arena);
        struct freqData *fdata;

        int a, b;
        int newPercentage;
        double structureTotalInitialHealth;

        if (!ad->running) return;

        for (a = 0; a < STRUCTURES_PLAYINGFREQS; a++)
        {
                fdata = GetFreqData(arena, a);

                if (fdata->config.percentageDisplay_imageStart < 0 || fdata->config.percentageDisplay_objectStart < 0)
                        continue;

                structureTotalInitialHealth = (double) fdata->structureTotalInitialHealth;

                if (structureTotalInitialHealth)
                {
                        newPercentage = fdata->structureTotalHealth / structureTotalInitialHealth * 100;

                        if (!newPercentage && fdata->structureTotalHealth)
                                newPercentage = 1; //make sure 0% really means everything is destroyed
                }
                else
                {
                        newPercentage = 0;
                }

                if (newPercentage != fdata->percentageDisplay_percentageLVZ) //no need to update
                {
                        short id[4]; // 100% = 4 character
                        char ons[4];

                        int digit;
                        int value = newPercentage;

                        for (b = 1; b < 4; b++) // 100 = 3 characters
                        {
                                id[b-1] = fdata->config.percentageDisplay_objectStart + b;

                                if (value == 0 && b != 1) //no leading zero's
                                {
                                        ons[b-1] = 0;
                                }
                                else
                                {
                                        digit = value % 10;
                                        value /= 10;
                                        ons[b-1] = 1;
                                        util->ObjImage(arena, id[b-1], fdata->config.percentageDisplay_imageStart + digit, true);
                                }
                        }

                        if (fdata->config.percentageDisplay_imagePercentage >= 0)
                        {
                                id[3] = fdata->config.percentageDisplay_objectStart;
                                ons[3] = 1;
                                util->ObjImage(arena, id[3], fdata->config.percentageDisplay_imagePercentage, true);
                                util->ObjToggleSet(arena, id, ons, 4, true);
                        }
                        else
                        {
                                util->ObjToggleSet(arena, id, ons, 3, true);
                        }

                        fdata->percentageDisplay_percentageLVZ = newPercentage;

                }
        }
}

static void StructureTileDamage(Arena *arena, int x, int y, Player *firedBy, int damage, int wtype, int level, bool bouncingbomb, int emptime, void *clos)
{
        struct structureData *sdata = clos;
        assertlm(sdata);
        assertlm(arena);
        assertlm(arena == sdata->arena);
        struct arenaData *ad = GetArenaData(arena);
        assertlm(ad->initialized);
	int freq = firedBy->p_freq;
	int onfreq;

        ticks_t now = current_ticks();

        Link *link;
        Player *p;
        struct freqData *fdata = GetFreqData(arena, sdata->config.freq);

        //int oldhealth, newhealth;

        if (freq == sdata->config.freq && !sdata->config.teamDamage) //no friendly fire if teamDamage = false
                return;

        if (!sdata->alive)
                return;

        MIN(damage, 0);

	if (ad->config.weightedDamage)
	{
		onfreq = 0;
		pd->Lock();
		FOR_EACH_PLAYER(p)
		{
			if (p->arena == arena && IS_HUMAN(p) && p->p_freq ==freq)
			{
				onfreq++;
			}
		}
		pd->Unlock();
		MIN(onfreq, 1);
		damage /= onfreq;
	}

        int newHealth = sdata->health - damage;

        MIN(newHealth, 0);

        DO_CBS(CB_STRUCTURES_STRUCTUREDAMAGED, arena, StructuresStructureDamagedFunc, (sdata, newHealth));

        SetHealth(sdata, newHealth, false); //building may not be alive after this point


        if (sdata->config.freq < STRUCTURES_FREQS && sdata->config.isBase)
        {

                if (fdata->config.underAttackReplayTicks &&
                                (!fdata->lastUnderAttack || TICK_DIFF(now, fdata->lastUnderAttack) >= fdata->config.underAttackReplayTicks))
                {
                        pd->Lock();
                        FOR_EACH_PLAYER(p)
                        {
                                if (p->arena == arena && IS_HUMAN(p) &&
                                                p->p_freq == sdata->config.freq)
                                {
                                        chat->SendSoundMessage(p, fdata->config.underAttackSound, "%s", fdata->config.underAttackMessage);
                                }
                        }
                        pd->Unlock();

                        fdata->lastUnderAttack = now;

                }
        }
}

static int Cstructuresoptimalization_maxResults = 0;
static Player *Cstructuresoptimalization_p = NULL;

static int Cstructuresoptimalization_HashEnum_structures(const char *key, void *val, void *clos)
{
        struct structureData *sdata = val;
        int *b = clos;

        if (*b >= Cstructuresoptimalization_maxResults)
                return 0;


        if (Cstructuresoptimalization_checkCoordsMap(Cstructuresoptimalization_p, sdata->typeID, sdata->config.objectidLT100, sdata->config.x, sdata->config.y))
                (*b)++;

	if (*b >= Cstructuresoptimalization_maxResults)
                return 0;

        if (Cstructuresoptimalization_checkCoordsMap(Cstructuresoptimalization_p, sdata->typeID, sdata->config.objectidLT50, sdata->config.x, sdata->config.y))
                (*b)++;

	if (*b >= Cstructuresoptimalization_maxResults)
                return 0;


        if (Cstructuresoptimalization_checkCoordsMap(Cstructuresoptimalization_p, sdata->typeID, sdata->config.objectidLT25, sdata->config.x, sdata->config.y))
                (*b)++;

	if (*b >= Cstructuresoptimalization_maxResults)
                return 0;


        if (Cstructuresoptimalization_checkCoordsMap(Cstructuresoptimalization_p, sdata->typeID, sdata->config.destroyedObjectid, sdata->config.x + sdata->config.destroyedXOffset, sdata->config.y + sdata->config.destroyedYOffset))
                (*b)++;

	if (*b >= Cstructuresoptimalization_maxResults)
                return 0;

        if (Cstructuresoptimalization_checkCoordsMap(Cstructuresoptimalization_p, sdata->typeID, sdata->config.explosionObjectid, sdata->config.x + sdata->config.explosionXOffset, sdata->config.y + sdata->config.explosionYOffset))
                (*b)++;

        return 0;
}

static void Cstructuresoptimalization(const char *tc, const char *params, Player *p, const Target *target)
{

        if (!p || !p->arena) return;
        Arena *arena = p->arena;
        int maxResults = atoi(params); //returns 0 on failure

        if (!maxResults) maxResults = 20;

        Cstructuresoptimalization_maxResults = maxResults;
        Cstructuresoptimalization_p = p;

        if (!objs)
        {
                chat->SendCmdMessage(p, "Error: objects interface missing");
                return;
        }

        struct arenaData *ad = GetArenaData(arena);
        if (!ad->initialized)
        {
        	chat->SendCmdMessage(p, "Structures is not loaded here...");
        }

        //struct structureData *sdata;

        //int a;
        int b = 0; //b counts the results so far

        HashEnum(ad->structures, Cstructuresoptimalization_HashEnum_structures, &b);

        if (b >= maxResults)
        {
                chat->SendCmdMessage(p, "Maximum results reached (%d)...", b);
        }

        if (!b)
                chat->SendCmdMessage(p, "No results...");
}

static bool Cstructuresoptimalization_checkCoordsMap(Player *p, const char *typeID, int objectid, int configX, int configY)
{

        //Check if the coordinates of a specific object id defaults matches the one in the settings; returns true when a result is sent to the player
        //This function is only used by Cstructuresoptimalization to avoid lots of duplicate code
        assertlm(p);
        assertlm(p->arena);

        if (objectid >= 0 && objs)
        {
                int x = -1;
                int y = -1;
                int mapobj = -1;

                //(Arena *arena, int id, int *off, int *image, int *layer, int *mode, int *mapobj, int *x, int *y, int *rx, int *ry)
                if (objs->InfoDefault(p->arena, objectid, NULL, NULL, NULL, NULL, &mapobj, &x, &y, NULL, NULL))
                {
                        if (x == -1 || y == -1 || mapobj == -1)
                        {
                                chat->SendCmdMessage(p, "Error (%s): Something went wrong in the objects2 module while getting the info for objectid %d.", typeID, objectid);
                        }

                        if (mapobj)
                        {
                                if (x != configX || y != configY)
                                {
                                        chat->SendCmdMessage(p, "Optimalization (%s): Give objectid %d the coordinates \t%d,\t%d", typeID, objectid, configX, configY);
                                        return true;
                                }
                        }
                        else
                        {
                                chat->SendCmdMessage(p, "Error (%s): objectid %d is not a map object.", typeID, objectid);
                                return true;
                        }
                }
                else
                {
                        chat->SendCmdMessage(p, "Error (%s): objectid %d does not exist.", typeID, objectid);
                        return true;
                }
        }
        return false;
}

/** Get the arena data */
static struct arenaData* GetArenaData(Arena *arena)
{
        assertlm(arena);
        assertlm(arenaKey != -1);
        struct arenaDataContainer* adCont = P_ARENA_DATA(arena, arenaKey);
        assertlm(adCont);
        assertlm(adCont->realArenaData);

        return adCont->realArenaData;
}
static const struct arenaData* GetArenaDataInterface(Arena *arena)
{
        return GetArenaData(arena);
}

static struct freqData* GetFreqData(Arena *arena, int freq)
{
        assertlm(arena);
        assertlm(freq < STRUCTURES_FREQS);
        struct arenaData *ad = GetArenaData(arena);

        return &ad->freqs[freq];
}
static const struct freqData* GetFreqDataInterface(Arena *arena, int freq)
{
        return GetFreqData(arena, freq);
}
static struct structureData* GetStructureData(Arena *arena, const char* typeID_)
{
        assertlm(arena);
        assertlm(typeID_);
        struct arenaData *ad = GetArenaData(arena);

        return HashGetOne(ad->structures, typeID_);
}
static const struct structureData* GetStructureDataInterface(Arena *arena, const char* typeID_)
{
        return GetStructureData(arena, typeID_);
}
static struct structureTypeData* GetStructureTypeData(Arena *arena, int freq, STRUCTURETYPE type)
{
        assertlm(arena);
        assertlm(freq >= 0 && freq < STRUCTURES_FREQS);
        assertlm(type < STRUCTURES_STRUCTURETYPES);
        struct arenaData *ad = GetArenaData(arena);
        return &ad->structureTypes[freq][type];
}

static const struct structureTypeData* GetStructureTypeDataInterface(Arena *arena, int freq, STRUCTURETYPE type)
{
        return GetStructureTypeData(arena, freq, type);
}

static void SetRespawnRate(Arena *arena, int freq, unsigned int value)
{


        assertlm(arena);
        struct arenaData *ad = GetArenaData(arena);

        if (freq >= 0 && freq < STRUCTURES_FREQS)
        {

                ad->freqs[freq].respawnRate = value;

        }
}
static void SetRepairRate(Arena *arena, int freq, unsigned int value)
{


        assertlm(arena);
        struct arenaData *ad = GetArenaData(arena);

        if (freq >= 0 && freq < STRUCTURES_FREQS)
        {
                ad->freqs[freq].repairRate = value;
        }
}


struct DoDamage_HashEnum_structures_clos
{
	int x;
	int y;
	int damage;
	int radius;
	int immuneFreq;
};
static int DoDamage_HashEnum_structures(const char *key, struct structureData *sdata, struct DoDamage_HashEnum_structures_clos *clos)
{
	if (!sdata->alive || sdata->config.freq == clos->immuneFreq)
		return 0;

	int x = sdata->config.x + sdata->config.width / 2;
	int y = sdata->config.y + sdata->config.height / 2;
	int dist = util->LHypot(clos->x - x, clos->y - y);
	if (dist < clos->radius)
	{
		int damagedealt = dist * - clos->damage / clos->radius + clos->damage;
		int newHealth = sdata->health - damagedealt;
		MIN(newHealth, 0);
		DO_CBS(CB_STRUCTURES_STRUCTUREDAMAGED, sdata->arena, StructuresStructureDamagedFunc, (sdata, newHealth));
		SetHealth(sdata, newHealth, false); //building may not be alive after this point
	}
	return 0;
}
static void DoDamage(Arena *arena, int x, int y, int damage, int radius, int immuneFreq)
{
	assertlm(arena);
        struct arenaData *ad = GetArenaData(arena);
	if (!ad->initialized || !radius) return;

	struct DoDamage_HashEnum_structures_clos clos = {x, y, damage, radius, immuneFreq};
	HashEnum(ad->structures, (int (*)(const char *, void *, void *))DoDamage_HashEnum_structures, &clos);
}

EXPORT const char info_structures[] = "Structures v2 by JoWie. (Rewritten from structures v1 by Smong) ("BUILDDATE")\n";
static void ReleaseInterfaces()
{
        mm->ReleaseInterface(lm);
        mm->ReleaseInterface(aman);
        mm->ReleaseInterface(cfg);
        mm->ReleaseInterface(ml);
        mm->ReleaseInterface(damage);
        mm->ReleaseInterface(mapdata);
        mm->ReleaseInterface(cmd);
        mm->ReleaseInterface(objs);
        mm->ReleaseInterface(chat);
        mm->ReleaseInterface(pd);
        mm->ReleaseInterface(stats);
        mm->ReleaseInterface(game);
        mm->ReleaseInterface(util);
        mm->ReleaseInterface(attrman);
}

EXPORT int MM_structures (int action, Imodman *mm_, Arena *arena)
{
        if (action == MM_LOAD)
        {
                mm      = mm_;
                lm      = mm->GetInterface(I_LOGMAN      , ALLARENAS);
                aman    = mm->GetInterface(I_ARENAMAN    , ALLARENAS);
                cfg     = mm->GetInterface(I_CONFIG      , ALLARENAS);
                ml      = mm->GetInterface(I_MAINLOOP    , ALLARENAS);
                damage  = mm->GetInterface(I_DAMAGE      , ALLARENAS);
                mapdata = mm->GetInterface(I_MAPDATA     , ALLARENAS);
                cmd     = mm->GetInterface(I_CMDMAN      , ALLARENAS);
                objs    = mm->GetInterface(I_OBJECTS     , ALLARENAS);
                chat    = mm->GetInterface(I_CHAT        , ALLARENAS);
                pd      = mm->GetInterface(I_PLAYERDATA  , ALLARENAS);
                stats   = mm->GetInterface(I_STATS       , ALLARENAS); //optional
                game    = mm->GetInterface(I_GAME        , ALLARENAS);
                util    = mm->GetInterface(I_UTIL        , ALLARENAS);
                attrman = mm->GetInterface(I_ATTRMAN     , ALLARENAS);

                if (!lm || !aman || !cfg || !ml || !damage || !mapdata || !cmd || !objs || !chat || !pd || !game || !util || !attrman)
                {
                        if (lm)
                                lm->Log(L_ERROR, "<structures> Missing Interface\n");
                        else
                                printf("<structures> Missing Interface\n");

                        ReleaseInterfaces();
                        return MM_FAIL;
                }

                if (!stats)
                        lm->Log(L_WARN, "<structures> stats module not loaded");


                arenaKey = aman->AllocateArenaData(sizeof(struct arenaDataContainer));

                if (arenaKey == -1)
                {
                        ReleaseInterfaces();
                        return MM_FAIL;
                }

		attrman->Lock();
                settingSetter = attrman->RegisterSetter();
                attrman->UnLock();

                return MM_OK;
        }
        else if (action == MM_UNLOAD)
        {
                if (swapgameInterface.head.arena_registrations)
                        return MM_FAIL;

		if (structuresInterface.head.arena_registrations)
                        return MM_FAIL;

		attrman->Lock();
		attrman->UnregisterSetter(settingSetter);
		attrman->UnLock();

                aman->FreeArenaData(arenaKey);

                ReleaseInterfaces();
                return MM_OK;
        }
        else if (action == MM_ATTACH)
        {
                //allocate the arena data, a container is used to prevent wasting memory where the module is not attached
                struct arenaDataContainer* adCont = P_ARENA_DATA(arena, arenaKey);
                assertlm(adCont);
                adCont->realArenaData = amalloc(sizeof(struct arenaData));

		mm->RegInterface(&structuresInterface, arena);
                mm->RegInterface(&swapgameInterface, arena);
                //mm->RegCallback(CB_ARENAACTION, ArenaAction, arena);

                InitializeArena(arena);

                return MM_OK;
        }
        else if (action == MM_DETACH)
        {
                if (mm->UnregInterface(&swapgameInterface, arena))
                	return MM_FAIL;

                if (mm->UnregInterface(&structuresInterface, arena))
                        return MM_FAIL;
                //mm->UnregCallback(CB_ARENAACTION, ArenaAction, arena);

                struct arenaData *ad = GetArenaData(arena);

                if (ad->running)
                        StopGame(arena);

                CleanupArena(arena);

                if (arenaKey != -1)
                {
                        //free the arena data
                        struct arenaDataContainer* adCont = P_ARENA_DATA(arena, arenaKey);
                        assertlm(adCont);
                        afree(adCont->realArenaData);
                        adCont->realArenaData = NULL;
                }
                return MM_OK;
        }

        return MM_FAIL;
}
#undef tFREE
