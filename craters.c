/**
        Craters
        Places a lvz image when player die
*/

#include "asss.h"
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "cncutil.h"

static Imodman     *mm;
static Iconfig     *cfg;
static Iutil       *util;
static Iarenaman   *aman;

static int arenaKey = -1;

struct arenaData
{
        bool attached;
        int ObjectTicker;
        int flames_ObjectTicker;
        int flames;

        struct
        {
                int XOffset;
                int YOffset;
                int ObjectStart;
                int ObjectsAllocated;

                int flames_occurrence;
                int flames_XOffset;
                int flames_YOffset;
                int flames_ObjectStart;
                int flames_ObjectsAllocated;
        } config;
};

static struct arenaData* GetArenaData(Arena *arena)
{
        assertlm(arena);
        assertlm(arenaKey != -1);
        struct arenaData* adata = P_ARENA_DATA(arena, arenaKey);
        assertlm(adata);
        return adata;
}

static void ReadConfig(Arena *arena)
{
        struct arenaData* adata = GetArenaData(arena);
        ConfigHandle ch = arena->cfg;

        adata->config.XOffset = cfg->GetInt(ch, "craters", "xoffset", 0);
        adata->config.YOffset = cfg->GetInt(ch, "craters", "yoffset", 0);
        adata->config.ObjectStart = cfg->GetInt(ch, "craters", "objectstart", -1);
        adata->config.ObjectsAllocated = cfg->GetInt(ch, "craters", "objectsallocated", 0);

        adata->config.flames_occurrence = cfg->GetInt(ch, "craters", "flames_occurrence", 0);
        adata->config.flames_XOffset = cfg->GetInt(ch, "craters", "flames_xoffset", 0);
        adata->config.flames_YOffset = cfg->GetInt(ch, "craters", "flames_yoffset", 0);
        adata->config.flames_ObjectStart = cfg->GetInt(ch, "craters", "flames_objectstart", -1);
        adata->config.flames_ObjectsAllocated = cfg->GetInt(ch, "craters", "flames_objectsallocated", 0);
}

static void KillCB(Arena *arena, Player *killer, Player *killed, int bounty, int flags, int pts, int green)
{
        struct arenaData* adata = GetArenaData(arena);

        if (!adata->attached || !adata->config.ObjectsAllocated || adata->config.ObjectStart < 0)
                return;

        int objectId = adata->config.ObjectStart + adata->ObjectTicker;
        adata->ObjectTicker = (adata->ObjectTicker + 1) % adata->config.ObjectsAllocated;
        int x = killed->position.x + adata->config.XOffset;
        int y = killed->position.y + adata->config.YOffset;

        util->ObjMove(arena, objectId, x, y, 0, 0, false);
        util->ObjToggle(arena, objectId, 1, false);



        if (!adata->config.flames_occurrence || !adata->config.flames_ObjectsAllocated || adata->config.flames_ObjectStart < 0)
                return;

        adata->flames++;
        if (adata->flames == adata->config.flames_occurrence)
        {
                int flames_objectId = adata->config.flames_ObjectStart + adata->flames_ObjectTicker;
                adata->flames_ObjectTicker = (adata->flames_ObjectTicker + 1) % adata->config.flames_ObjectsAllocated;
                int x = killed->position.x + adata->config.flames_XOffset;
                int y = killed->position.y + adata->config.flames_YOffset;

                util->ObjMove(arena, flames_objectId, x, y, 0, 0, false);
                util->ObjToggle(arena, flames_objectId, 1, false);
                adata->flames = 0;
        }
}


EXPORT const char info_craters[] = "Craters by JoWie ("BUILDDATE")\n";
static void ReleaseInterfaces()
{
        mm->ReleaseInterface(cfg   );
        mm->ReleaseInterface(util  );
        mm->ReleaseInterface(aman  );
}

EXPORT int MM_craters(int action, Imodman *mm_, Arena *arena)
{
        if (action == MM_LOAD)
        {
                mm = mm_;

                cfg    = mm->GetInterface(I_CONFIG    , ALLARENAS);
                util   = mm->GetInterface(I_UTIL      , ALLARENAS);
                aman   = mm->GetInterface(I_ARENAMAN  , ALLARENAS);

                if (!cfg || !util || !aman)
                {
                        printf("<craters> Missing Interface\n");

                        ReleaseInterfaces();

                        return MM_FAIL;
                }

                arenaKey = aman->AllocateArenaData(sizeof(struct arenaData));

                if (arenaKey == -1)
                {
                        ReleaseInterfaces();
                        return MM_FAIL;
                }

                return MM_OK;
        }
        else if (action == MM_UNLOAD)
        {
                aman->FreeArenaData(arenaKey);
                mm->UnregCallback(CB_KILL, KillCB, ALLARENAS);

                ReleaseInterfaces();
                return MM_OK;
        }
        else if (action == MM_ATTACH)
        {
                struct arenaData* adata = GetArenaData(arena);
                adata->attached = true;

                mm->RegCallback(CB_KILL, KillCB, arena);

                ReadConfig(arena);

                return MM_OK;
        }
        else if (action == MM_DETACH)
        {
                struct arenaData* adata = GetArenaData(arena);
                adata->attached = false;

                mm->UnregCallback(CB_KILL, KillCB, arena);

                return MM_OK;
        }

        return MM_FAIL;
}
