/* $Id: swappablegame.h 3 2008-03-21 23:05:13Z joris $ */
/* this is a generic interface for swappable games. a swappable game is a game
 * type module that can be loaded/unloaded while an arena is still running, so
 * you can rotate through several game types without changing arena. */

#ifndef __SWAPPABLEGAME_H
#define __SWAPPABLEGAME_H

/** the callback function when a game has been won. tgt contains the winners
 * or NULL if there were none. */
typedef void (*GameoverFunc)(Arena *arena, const Target *tgt);

/** the fg_ctf interface struct */
typedef struct Iswappablegame
{
	INTERFACE_HEAD_DECL

	void (*StartGame)(Arena *arena, GameoverFunc func);
	void (*StopGame)(Arena *arena);
} Iswappablegame;

#endif

