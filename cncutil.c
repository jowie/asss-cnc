#include <stdio.h>
#include <string.h>
#include "cncutil.h"
#include "defs.h"
#include "mainloop.h"
#include "objects.h"

#ifdef NDEBUG
#define assertlm_(e)	       ((void)0)
#else
#define assertlm_(e)       ((e) ? (void)0 : AssertLM(#e, __FILE__, __func__, __LINE__))
#endif

static Imodman      *mm;
static Ilogman      *lm;
static Iplayerdata  *pd;
static Ichat        *chat;
static Igame        *game;
static Iarenaman    *aman;
static Iconfig      *cfg;
static Imainloop    *ml;

static const char *GetShipName(unsigned int ship);
static const char *GetCustomShipName(Arena *arena, unsigned int ship);
static long LHypot (register long dx, register long dy);
static void AssertLM (const char* e, const char* file, const char *function, int line);
static void FunctionLogLM(const char *module, int line, const char *func, const char *info);
static void SFXToNearbyPlayers (Arena *arena, int sound, int x, int y, int width, int height, int range);
static void PrizeNearbyPlayers (Arena *arena, int prize, int x, int y, int width, int height, int range);
static void GetArenaSet(LinkedList *set, Arena *arena);
static void GetFreqSet(LinkedList *set, Arena *arena, int freq);
static void GetNotonFreqSet(LinkedList *set, Arena *arena, int freq);
static char* cfgGetStrAlloc(ConfigHandle ch, const char *section, const char *key);
static char* strcpyalloc(const char *s);
static void ArenaAction(Arena *arena, int action);
static void ReadConfig(Arena *arena);
static void CleanupArena(Arena *arena);
static void ReleaseInterfaces();
static void ObjToggle(Arena *arena, int id, int on, bool saveState);
static void ObjToggleSet(Arena *arena, short *id, char *ons, int size, bool saveState);
static void ObjMove(Arena *arena, int id, int x, int y, int rx, int ry, bool saveState);
static void ObjImage(Arena *arena, int id, int image, bool saveState);
static void ObjLayer(Arena *arena, int id, int layer, bool saveState);
static void ObjTimer(Arena *arena, int id, int time, bool saveState);
static void ObjMode(Arena *arena, int id, int mode, bool saveState);

static Iutil utilInterface =
{
        INTERFACE_HEAD_INIT(I_UTIL, "util")

        LHypot,

        GetShipName,
        GetCustomShipName,

        AssertLM,
        FunctionLogLM,

        SFXToNearbyPlayers,
        PrizeNearbyPlayers,

        GetArenaSet,
        GetFreqSet,
        GetNotonFreqSet,

        cfgGetStrAlloc,
        strcpyalloc,

        ObjToggle,
        ObjToggleSet,
        ObjMove,
        ObjImage,
        ObjLayer,
        ObjTimer,
        ObjMode
};

static int arenaKey = -1;
struct arenaData
{
        const char *customShipNames[9];
};

static const char *shipNames[9] =
{
        "Warbird",
        "Javelin",
        "Spider",
        "Leviathan",
        "Terrier",
        "Weasel",
        "Lancaster",
        "Shark",
        "Spectator"
};

static const char *GetShipName(unsigned int ship)
{
        assertlm_(ship < 9);
        return shipNames[ship];
}

static const char *GetCustomShipName(Arena *arena, unsigned int ship)
{
        assertlm_(ship < 9);
        struct arenaData* ad = P_ARENA_DATA(arena, arenaKey);

        if (ad->customShipNames[ship])
        {
                return ad->customShipNames[ship];
        }
        else
        {
                return GetShipName(ship);
        }
}

/** quick integer square root */
static long LHypot (register long dx, register long dy)
{
        register unsigned long r, dd;

        dd = dx*dx+dy*dy;

        if (dx < 0) dx = -dx;
        if (dy < 0) dy = -dy;

        /* initial hypotenuse guess (from Gems) */
        r = (dx > dy) ? (dx+(dy>>1)) : (dy+(dx>>1));

        if (r == 0) return (long)r;

        /* converge 3 times */
        r = (dd/r+r)>>1;
        r = (dd/r+r)>>1;
        r = (dd/r+r)>>1;

        return (long)r;
}

static void AssertLM (const char* e, const char* file, const char *function, int line)
{
        if (lm) lm->Log(L_ERROR | L_SYNC, "\nAssertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
        fullsleep(500); //sleep half a second to make sure the log arrives everywhere
        Error(EXIT_GENERAL, "\nAssertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
}

static void FunctionLogLM(const char *module, int line, const char *func, const char *info)
{
        if (info)
                lm->Log(L_DRIVEL, "<%s> %s() on line %d : %s", module, func, line, info);
        else
                lm->Log(L_DRIVEL, "<%s> %s() on line %d", module, func, line);
}

static void SFXToNearbyPlayers (Arena *arena, int sound, int x, int y, int width, int height, int range)
{
        assertlm_(arena);

        Player *p;
        Link *link;
        int x1 ,y1, x2, y2;

        x1 = x - range;
        y1 = y - range;

        x2 = x + width + range;
        y2 = y + height + range;

        if (sound)
        {
                pd->Lock();
                FOR_EACH_PLAYER(p)
                {
                        if (p->arena == arena && IS_HUMAN(p) &&
                                        p->position.x >= x1 && p->position.x <= x2 &&
                                        p->position.y >= y1 && p->position.y <= y2)
                        {
                                chat->SendSoundMessage(p, sound, "%s", "");
                        }
                }
                pd->Unlock();
        }
}

static void PrizeNearbyPlayers (Arena *arena, int prize, int x, int y, int width, int height, int range)
{
        assertlm_(arena);

        Player *p;
        Link *link;
        Target tgt;
        tgt.type = T_PLAYER;

        int x1 ,y1, x2, y2;

        x1 = x - range;
        y1 = y - range;

        x2 = x + width + range;
        y2 = y + height + range;


        pd->Lock();
        FOR_EACH_PLAYER(p)
        {
                if (p->arena == arena && IS_HUMAN(p) &&
                                p->position.x >= x1 && p->position.x <= x2 &&
                                p->position.y >= y1 && p->position.y <= y2)
                {
                        tgt.u.p = p;
                        game->GivePrize(&tgt, prize, 1);
                }
        }
        pd->Unlock();
}

static void GetArenaSet(LinkedList *set, Arena *arena)
{
        Link *link;
        Player *p;

        pd->Lock();
        FOR_EACH_PLAYER(p)
        {
                if (p->status == S_PLAYING &&
                                (arena == ALLARENAS || p->arena == arena) )
                        LLAdd(set, p);
        }
        pd->Unlock();
}

static void GetFreqSet(LinkedList *set, Arena *arena, int freq)
{
        Link *link;
        Player *p;

        pd->Lock();
        FOR_EACH_PLAYER(p)
        {
                if (p->status == S_PLAYING &&
                                p->arena == arena &&
                                p->p_freq == freq)
                        LLAdd(set, p);
        }
        pd->Unlock();
}

static void GetNotonFreqSet(LinkedList *set, Arena *arena, int freq)
{
        Link *link;
        Player *p;

        pd->Lock();
        FOR_EACH_PLAYER(p)
        {
                if (p->status == S_PLAYING &&
                                p->arena == arena &&
                                p->p_freq != freq)
                        LLAdd(set, p);
        }
        pd->Unlock();
}


/*static char* cfgGetStrAlloc(ConfigHandle ch, const char *section, const char *key)
{
    char *ret = NULL;
    const char *s = cfg->GetStr(ch, section, key);
    size_t size;

    if (s)
    {
        size = strlen(s) + 1;
        if (size > 1)
        {
            ret = amalloc(size);
            strncpy(ret, s, size);
            *(ret + size - 1) = 0;
        }
    }

    return ret;
}*/

static char* cfgGetStrAlloc(ConfigHandle ch, const char *section, const char *key)
{
        const char *s = cfg->GetStr(ch, section, key);

        return strcpyalloc(s);
}

static char* strcpyalloc(const char *s)
{
        char *ret = NULL;
        int len;
        size_t size;

        if (s)
        {
        	len = strlen(s) + 1; //including \0
                size = len * sizeof(char);
                if (size > 0)
                {
                        ret = amalloc(size);
                        astrncpy(ret, s, len);
                }
        }

        return ret;
}


static void ArenaAction(Arena *arena, int action)
{
        if (action == AA_CREATE || action == AA_CONFCHANGED)
        {
                ReadConfig(arena);
        }
        else if (action == AA_DESTROY)
        {
                CleanupArena(arena);
        }
}

static void ReadConfig(Arena *arena)
{
        struct arenaData* ad = P_ARENA_DATA(arena, arenaKey);
        int a;
        ConfigHandle ch = arena->cfg;

        for (a = 0; a < 9; a++)
        {
                ad->customShipNames[a] = cfgGetStrAlloc(ch, GetShipName(a), "Name");
        }
}

static void CleanupArena(Arena *arena)
{
        struct arenaData* ad = P_ARENA_DATA(arena, arenaKey);
        int a;

        for (a = 0; a < 9; a++)
        {
                if (ad->customShipNames[a])
                        afree(ad->customShipNames[a]);
        }
}

#define tOBJ() \
        assertlm_(arena); \
        Iobjects *objs = mm->GetInterface(I_OBJECTS, ALLARENAS); \
        Target tgt; \
        if (saveState) \
        { \
                tgt.type = T_ARENA; \
                tgt.u.arena = arena; \
                FN; \
        } \
        else \
        { \
                tgt.type = T_PLAYER; \
                Link *link; \
                pd->Lock(); \
                FOR_EACH_PLAYER(tgt.u.p) \
                { \
                        if (tgt.u.p->arena == arena) \
                                FN; \
                } \
                pd->Unlock(); \
        } \
        mm->ReleaseInterface(objs);


static void ObjToggle(Arena *arena, int id, int on, bool saveState)
{
#define FN objs->Toggle(&tgt, id, on)
        tOBJ()
#undef FN
}

static void ObjToggleSet(Arena *arena, short *id, char *ons, int size, bool saveState)
{
#define FN objs->ToggleSet(&tgt, id, ons, size)
        tOBJ()
#undef FN
}

static void ObjMove(Arena *arena, int id, int x, int y, int rx, int ry, bool saveState)
{
#define FN objs->Move(&tgt, id, x, y, rx, ry)
        tOBJ()
#undef FN
}

static void ObjImage(Arena *arena, int id, int image, bool saveState)
{
#define FN objs->Image(&tgt, id, image)
        tOBJ()
#undef FN
}

static void ObjLayer(Arena *arena, int id, int layer, bool saveState)
{
#define FN objs->Layer(&tgt, id, layer)
        tOBJ()
#undef FN
}

static void ObjTimer(Arena *arena, int id, int time, bool saveState)
{
#define FN objs->Timer(&tgt, id, time)
        tOBJ()
#undef FN
}

static void ObjMode(Arena *arena, int id, int mode, bool saveState)
{
#define FN objs->Mode(&tgt, id, mode)
        tOBJ()
#undef FN
}

static void ReleaseInterfaces()
{
        mm->ReleaseInterface(lm);
        mm->ReleaseInterface(chat);
        mm->ReleaseInterface(pd);
        mm->ReleaseInterface(game);
        mm->ReleaseInterface(aman);
        mm->ReleaseInterface(cfg);
        mm->ReleaseInterface(ml);
}

EXPORT int MM_util (int action, Imodman *mm_, Arena *arena)
{
        if (action == MM_LOAD)
        {
                mm      = mm_;
                lm      = mm->GetInterface(I_LOGMAN      , ALLARENAS);
                chat    = mm->GetInterface(I_CHAT        , ALLARENAS);
                pd      = mm->GetInterface(I_PLAYERDATA  , ALLARENAS);
                game    = mm->GetInterface(I_GAME        , ALLARENAS);
                aman    = mm->GetInterface(I_ARENAMAN    , ALLARENAS);
                cfg     = mm->GetInterface(I_CONFIG      , ALLARENAS);
                ml      = mm->GetInterface(I_MAINLOOP    , ALLARENAS);

                if (!lm || !chat || !pd || !game || !aman || !cfg)
                {
                        if (lm)
                                lm->Log(L_ERROR, "<util> Missing Interface\n");
                        else
                                printf("<util> Missing Interface\n");

                        ReleaseInterfaces();
                        return MM_FAIL;
                }

                arenaKey = aman->AllocateArenaData(sizeof(struct arenaData));
                if (arenaKey == -1)
                {
                        ReleaseInterfaces();
                        return MM_FAIL;
                }

                mm->RegCallback(CB_ARENAACTION, ArenaAction, ALLARENAS);

                mm->RegInterface(&utilInterface, ALLARENAS);

                return MM_OK;
        }
        else if (action == MM_UNLOAD)
        {
                if (mm->UnregInterface(&utilInterface, ALLARENAS))
                        return MM_FAIL;

                mm->UnregCallback(CB_ARENAACTION, ArenaAction, ALLARENAS);

                aman->FreeArenaData(arenaKey);

                ReleaseInterfaces();
                return MM_OK;
        }

        return MM_FAIL;
}
