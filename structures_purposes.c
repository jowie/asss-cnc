
#include <stdio.h>
#include "asss.h"
#include "objects.h"
#include "structures.h"
#include "clientset.h"
#include "staticturret.h"
#include "cncutil.h"
#include "attrman.h"

/*
	structure purposes for the structures module

	[structure_purposes]
	RadarDisabledMapZoomFactor = 3
	DisabledShipExempt = 5

	;in 0.1%
	ReducedEnergyFactor = 900

	6 juli 2007 - Created by JoWie


*/

#define MODULE "structures_purposes"

// Interfaces
static Imodman *mm;
static Iarenaman *aman;
static Iobjects *objs;
static Iattrman *attrman;
static Iplayerdata *pd;
static Iconfig *cfg;
static Ilogman *lm;
static Ichat *chat;
static Iprng *prng;
static Iutil *util;

static int arenaKey = -1;
static AttrmanSetter setter;

struct arenaData
{
	Istructures *structures;

        bool gameRunning;

        int radardown_mapzoomreduction;

        int oldDisabledShip; //disabled ship last game
        int disabledShipExempt; //this ship can not be disabled
        int disabledShip; //currently disabled ship

        double reducedenergyfactor; // 0 = 0%; 1 = 100%

        int siloCreditStorage;
};

static void ReadSettings(Arena *arena);
static void GameStart (Arena *arena);
static void GameStop (Arena *arena);
static struct arenaData* GetArenaData(Arena *arena);
static shipmask_t AEGetAllowableShips(Player *p, int ship, int freq, char *err_buf, int buf_len);
static int AECanChangeFreq(Player *p, int new_freq, char *err_buf, int buf_len);

static Aenforcer enforcer =
{
	ADVISER_HEAD_INIT(A_ENFORCER)
	AEGetAllowableShips,
	AECanChangeFreq
};

static struct arenaData* GetArenaData(Arena *arena)
{
        assertlm(arena);
        assertlm(arenaKey != -1);
        struct arenaData* ad = P_ARENA_DATA(arena, arenaKey);
        assertlm(ad);
        return ad;
}

static void ReadSettings(Arena *arena)
{

        struct arenaData *ad = GetArenaData(arena);
        ConfigHandle ch = arena->cfg;

        ad->radardown_mapzoomreduction = cfg->GetInt(ch, "structures_purposes", "RadarDisabledMapZoomReduction", 0);
        ad->disabledShipExempt = cfg->GetInt(ch, "structures_purposes", "DisabledShipExempt", 0) - 1;
        ad->reducedenergyfactor = cfg->GetInt(ch, "structures_purposes", "ReducedEnergyFactor", 0) / 1000.0;
        ad->siloCreditStorage = cfg->GetInt(ch, "gamecredits", "MaxCreditsPerSilo", 0);

        if (ad->disabledShipExempt < 0 || ad->disabledShipExempt > 7)
                ad->disabledShipExempt = -1;

        ad->disabledShip = -1;
        ad->oldDisabledShip = -1;
}

static void GameStart (Arena *arena)
{

        struct arenaData *ad = GetArenaData(arena);
        int a;

        ad->gameRunning = true;

        a = 0;
        do
        {
                ad->disabledShip = prng->Number(0, 7); //get random number 0 t/m 7
                a++;

                if (a >= 1000)
                {
                        //abort the loop after having tried 1000 times
                        ad->disabledShip = 1;
                        break;
                }
        }
        while (ad->disabledShip == ad->disabledShipExempt || ad->disabledShip == ad->oldDisabledShip);

        ad->oldDisabledShip = ad->disabledShip;
}

static void GameStop (Arena *arena)
{

        struct arenaData *ad = GetArenaData(arena);
        ad->gameRunning = false;
}

static void StructureCount0to1(struct structureTypeData *stdata)
{

        assertlm(stdata);
        assertlm(stdata->arena);

        Arena *arena = stdata->arena;
        struct arenaData* adata = GetArenaData(arena);
        Istaticturret *staticturret;

        Target tgtFreq;
        tgtFreq.type = T_FREQ;
        tgtFreq.u.freq.arena = arena;
        tgtFreq.u.freq.freq = stdata->freq;

        switch (stdata->type)
        {
                case CONSTRUCTIONYARD:

			staticturret = mm->GetInterface(I_STATICTURRET, arena);
			if (staticturret)
				staticturret->FreezeRespawn(arena, stdata->freq, false);
			mm->ReleaseInterface(staticturret);

                        adata->structures->SetRespawnRate(arena, stdata->freq, 1000);

			break;
                case POWERPLANT:

                        break;
                case OREREFINERY:

                        break;
                case BARRACKS:

			attrman->Lock();
#define tSETVALUE(SHIP) \
        attrman->SetValue(&tgtFreq, setter, "cset::" #SHIP ":InitialEnergy", true, ATTRMAN_NO_SHIP, 0);

                        tSETVALUE(Warbird);
                        tSETVALUE(Javelin);
                        tSETVALUE(Spider);
                        tSETVALUE(Leviathan);
                        tSETVALUE(Terrier);
                        tSETVALUE(Weasel);
                        tSETVALUE(Lancaster);
                        tSETVALUE(Shark);

#undef tSETVALUE
			attrman->UnLock();

                        break;
                case WARFACTORY:

                        break;
                case AIRFIELD:

                        break;
                case RADAR:

                        //set normal radar
                        attrman->Lock();
                        attrman->SetValue(&tgtFreq, setter, "cset::Radar:MapZoomFactor", true, ATTRMAN_NO_SHIP, 0);
                        attrman->UnLock();

                        break;
                case TECH:

                        //adata->structures->SetRepairRate (arena, stdata->freq, 1000);

                        break;
                case SILO:

                        break;
                case REPAIR:

                        break;

        }
}
//static void StructureDestroyed (Arena *arena, struct structuredata *sdata)
static void StructureCount1to0(struct structureTypeData *stdata)
{

        assertlm(stdata);
        assertlm(stdata->arena);

	Istaticturret *staticturret;
        Arena *arena = stdata->arena;
        struct arenaData *ad = GetArenaData(arena);
        Player *p;
        Link *link;

        Target tgtFreq;
        tgtFreq.type = T_FREQ;
        tgtFreq.u.freq.arena = arena;
        tgtFreq.u.freq.freq = stdata->freq;

        int InitialEnergy;

        switch (stdata->type)
        {
                case CONSTRUCTIONYARD:

                        ad->structures->SetRespawnRate (arena, stdata->freq, 0);

			staticturret = mm->GetInterface(I_STATICTURRET, arena);
			if (staticturret)
				staticturret->FreezeRespawn(arena, stdata->freq, true);
			mm->ReleaseInterface(staticturret);

                        break;
                case POWERPLANT:

                        break;
                case OREREFINERY:

                        break;
                case BARRACKS:
			
			attrman->Lock();
#define tSETVALUE(SHIP) \
        InitialEnergy = -((int) cfg->GetInt(arena->cfg, #SHIP, "MaximumEnergy", 0) * ad->reducedenergyfactor); \
        attrman->SetValue(&tgtFreq, setter, "cset::" #SHIP ":InitialEnergy", true, ATTRMAN_NO_SHIP, InitialEnergy);

                        tSETVALUE(Warbird);
                        tSETVALUE(Javelin);
                        tSETVALUE(Spider);
                        tSETVALUE(Leviathan);
                        tSETVALUE(Terrier);
                        tSETVALUE(Weasel);
                        tSETVALUE(Lancaster);
                        tSETVALUE(Shark);

#undef tSETVALUE
			attrman->UnLock();

                        break;
                case WARFACTORY:

                        if (ad->disabledShip >= 0 && ad->disabledShip <= 8)
                        {

                                pd->Lock();
                                FOR_EACH_PLAYER(p)
                                {
                                        if (p->arena == arena && p->p_freq == stdata->freq)
                                                chat->SendMessage(p, "%s lost.", util->GetCustomShipName(arena, ad->disabledShip));
                                }
                                pd->Unlock();
                        }

                        break;
                case AIRFIELD:

                        break;
                case RADAR:

			attrman->Lock();
                        attrman->SetValue(&tgtFreq, setter, "cset::Radar:MapZoomFactor", true, ATTRMAN_NO_SHIP, ad->radardown_mapzoomreduction);
                        attrman->UnLock();

                        break;
                case TECH:

                        //ad->structures->SetRepairRate(arena, stdata->freq, 0);

                case SILO:

                        break;
                case REPAIR:

                        break;
        }
}
static void StructuresTypeDataStructureCountChanged (struct structureTypeData *stdata, int oldStructureCount)
{
	assertlm(stdata);
        assertlm(stdata->arena);

	Target tgtFreq;
        tgtFreq.type = T_FREQ;
        tgtFreq.u.freq.arena = stdata->arena;
        tgtFreq.u.freq.freq = stdata->freq;

        struct arenaData *ad = GetArenaData(stdata->arena);

        if (stdata->structureCount == oldStructureCount) return;

        if (!oldStructureCount && stdata->structureCount)
        {
                StructureCount0to1(stdata);
        }
        else if (oldStructureCount && !stdata->structureCount)
        {
                StructureCount1to0(stdata);
        }

        if (stdata->type == POWERPLANT)
        {
		Istaticturret *staticturret = mm->GetInterface(I_STATICTURRET, stdata->arena);
		if (staticturret)
			staticturret->SetPower(stdata->arena, stdata->freq, stdata->structureCount);
		mm->ReleaseInterface(staticturret);
        }

        if (stdata->type == SILO)
        {
        	attrman->Lock();
        	attrman->SetValue(&tgtFreq, setter, "gamecredits::maxcredits", true, ATTRMAN_NO_SHIP, stdata->structureCount * ad->siloCreditStorage);
        	attrman->UnLock();
        }
}

static shipmask_t AEGetAllowableShips(Player *p, int ship, int freq, char *err_buf, int buf_len)
{
    assertlm(p->arena);
    Arena *arena = p->arena;
    struct arenaData *ad = GetArenaData(arena);
    structureTypeData *stdata_warfactory;

    if (freq >= 0 && freq < STRUCTURES_FREQS && ad->disabledShip >= 0)
    {
        stdata_warfactory = ad->structures->GetStructureTypeData(arena, freq, WARFACTORY);
        if (!stdata_warfactory->structureCount && stdata_warfactory->structureCountInitial) // war factory destroyed
        {
            if (ad->disabledShip == ship)
            {
                snprintf(err_buf, buf_len, "You can not use the %s because you lost your War Factory", util->GetCustomShipName(arena, ship));
            }

            return 255 ^ SHIPMASK(ad->disabledShip);
        }
    }

    return 255;
}

static int AECanChangeFreq(Player *p, int new_freq, char *err_buf, int buf_len)
{
    return 1;
}

EXPORT const char info_structures_purposes[] = "structures purposes by JoWie ("BUILDDATE")\n";

static void ReleaseInterfaces()
{
        mm->ReleaseInterface(aman);
        mm->ReleaseInterface(objs);
        mm->ReleaseInterface(attrman);
        mm->ReleaseInterface(pd);
        mm->ReleaseInterface(cfg);
        mm->ReleaseInterface(lm);
        mm->ReleaseInterface(chat);
        mm->ReleaseInterface(prng);
        mm->ReleaseInterface(util);
}

EXPORT int MM_structures_purposes(int action, Imodman *mm_, Arena *arena)
{
        if (action == MM_LOAD)
        {
                mm = mm_;

                aman            = mm->GetInterface(I_ARENAMAN, ALLARENAS);
                objs            = mm->GetInterface(I_OBJECTS, ALLARENAS);
                attrman         = mm->GetInterface(I_ATTRMAN, ALLARENAS);
                pd              = mm->GetInterface(I_PLAYERDATA, ALLARENAS);
                cfg             = mm->GetInterface(I_CONFIG, ALLARENAS);
                lm              = mm->GetInterface(I_LOGMAN, ALLARENAS);
                chat            = mm->GetInterface(I_CHAT, ALLARENAS);
                prng            = mm->GetInterface(I_PRNG, ALLARENAS);
                util            = mm->GetInterface(I_UTIL, ALLARENAS);

                if (!aman || !objs || !attrman || !pd || !cfg || !lm  || !chat || !prng || !util)
                {
                        ReleaseInterfaces();
                        return MM_FAIL;
                }

                arenaKey = aman->AllocateArenaData(sizeof(struct arenaData));

                if (arenaKey == -1)
                {
                        ReleaseInterfaces();
                        return MM_FAIL;
                }

		attrman->Lock();
                setter = attrman->RegisterSetter();
                attrman->UnLock();

                return MM_OK;
        }
        else if (action == MM_UNLOAD)
        {
                mm->UnregCallback(CB_STRUCTURES_GAMESTART, GameStart, ALLARENAS);
                mm->UnregCallback(CB_STRUCTURES_GAMESTOP, GameStop, ALLARENAS);
                mm->UnregCallback(CB_STRUCTURES_TYPEDATASTRUCTURECOUNTCHANGED, StructuresTypeDataStructureCountChanged, ALLARENAS);

		attrman->Lock();
                attrman->UnregisterSetter(setter);
                attrman->UnLock();
                aman->FreeArenaData(arenaKey);
                ReleaseInterfaces();

                return MM_OK;
        }
        else if (action == MM_ATTACH)
        {
                struct arenaData* adata = GetArenaData(arena);
                adata->structures = mm->GetInterface(I_STRUCTURES, arena);
                if (!adata->structures) return MM_FAIL;
                mm->RegAdviser(&enforcer, arena);

                mm->RegCallback(CB_STRUCTURES_GAMESTART, GameStart, arena);
                mm->RegCallback(CB_STRUCTURES_GAMESTOP, GameStop, arena);
                mm->RegCallback(CB_STRUCTURES_TYPEDATASTRUCTURECOUNTCHANGED, StructuresTypeDataStructureCountChanged, arena);

                ReadSettings(arena);

                return MM_OK;
        }
        else if (action == MM_DETACH)
        {
		struct arenaData* adata = GetArenaData(arena);
		mm->ReleaseInterface(adata->structures);
		mm->UnregAdviser(&enforcer, arena);

		mm->UnregCallback(CB_STRUCTURES_GAMESTART, GameStart, arena);
		mm->UnregCallback(CB_STRUCTURES_GAMESTOP, GameStop, arena);
		mm->UnregCallback(CB_STRUCTURES_TYPEDATASTRUCTURECOUNTCHANGED, StructuresTypeDataStructureCountChanged, arena);
                return MM_OK;
        }
        return MM_FAIL;
}
