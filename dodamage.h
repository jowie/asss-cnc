#ifndef DODAMAGE_H_INCLUDED
#define DODAMAGE_H_INCLUDED

#include <stdbool.h>
#include "packets/ppk.h"
#define I_DODAMAGE "dodamage-1"

/*
    Player *fakePlayer = fake->CreateFakePlayer(name, arena, SHIP_WARBIRD, 9999);
    fake->EndFaked(fakePlayer);
*/

/*
    Ways to hurt a player:

    Death: Spawn fake on top of him, fire level 4 thor with normal bombspeed.
    Splash Damage: Spawnfake on the specified location, fire specified bomb with no bomb speed, fake changes freq.
    Gun Damage: Attach fake on victim, fire the specified bullet.
*/


typedef struct Idodamage
{
    INTERFACE_HEAD_DECL

    /**
        Fires a weapon on the specified area.
        victims are the players who see the weapon.
        killer is the fake player firing the weapon.
        blocker is the fake player that sets off the explosion. Make sure the blocker and killer arent in the same freq
        Make sure the fake players are in the same arena as all the victims.

        x, y are the coordinates in pixels on the map.
        newFreq is the freq the fake player changes to, to cause the explosion. use -1 to disable this

        weaponType sets the type of the weapon, see packets/ppk.h
        weaponLevel is 0, 1, 2 or 3. weaponLevel can also be used for thors, where 0 is a normal thor.
        If you want EMP or Bouncing Bombs, set the fake player on a ship that has it.

        shrapnel is the number of shrapnels, between 0 and 31.
        shrapnelLevel is the gun level of the shrapnels. 0, 1, 2 or 3
        shrapelBouncing sets whether the shrapnel bounces
    */
    void (*MapDamage)(const Target *victims, Player *killer, Player *blocker, int x, int y, u16 weaponType, u16 weaponLevel, u16 shrapnel, u16 shrapnelLevel, u16 shrapelBouncing);

    // automatically create a fake player which is returned; The player will leave the arena after 4 seconds
    Player* (*MapDamageN)(Arena *arena, const Target *victims, const char *fakeName, int x, int y, u16 weaponType, u16 weaponLevel, u16 shrapnel, u16 shrapnelLevel, u16 shrapelBouncing);

    /**
        Hurts a player by firing a weapon directly at him.
        Make sure the fake player is in an unused / enemy freq.
        Make sure the fake player is in the same arena as all the victims.

        if warpOutSafe is set, the player(s) are warped out of safe zones before firing the weapon at him


        This might fail if the bomb / gun speed is very low or the player is moving at an extremely high speed. And the player warps before being hit.
    */
    void (*PlayerDamage)(const Target *victims, Player *killer, bool warpOutSafe, u16 weaponType, u16 weaponLevel, u16 shrapnel, u16 shrapnelLevel, u16 shrapelBouncing);

    // automatically create a fake player which is returned; The player will leave the arena after 4 seconds
    Player* (*PlayerDamageN)(Arena *arena, const Target *victims, const char *fakeName, bool warpOutSafe, u16 weaponType, u16 weaponLevel, u16 shrapnel, u16 shrapnelLevel, u16 shrapelBouncing);

} Idodamage;

#endif // DODAMAGE_H_INCLUDED
